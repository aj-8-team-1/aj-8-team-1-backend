FROM maven:3.8-openjdk-17-slim

RUN mkdir -p /app
WORKDIR /app

ADD pom.xml /app
ADD src /app/src

RUN mvn clean package
CMD ["java", "-jar", "/app/target/application-0.0.1-SNAPSHOT.jar"]