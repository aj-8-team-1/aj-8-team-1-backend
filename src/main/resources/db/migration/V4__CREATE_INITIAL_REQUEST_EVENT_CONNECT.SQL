CREATE TABLE event_request_tags
(
    id               SERIAL PRIMARY KEY,
    event_request_id INTEGER NOT NULL,
    tag_id           INTEGER NOT NULL,

    FOREIGN KEY (event_request_id) REFERENCES event_requests (id),
    FOREIGN KEY (tag_id) REFERENCES tags (id)
);

CREATE TABLE event_requests_websites
(
    id               SERIAL PRIMARY KEY,
    event_request_id INTEGER NOT NULL,
    websites_id      INTEGER NOT NULL,

    FOREIGN KEY (event_request_id) REFERENCES event_requests (id),
    FOREIGN KEY (websites_id) REFERENCES websites (id)
);