package application.controllers.user;

import application.exceptions.ApiError;
import application.models.user.Authority;
import application.models.user.User;
import application.services.user.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/tokens")
public class TokenController {
    private final UserService service;
    private final String jwtSecret;

    @Value("${jwt.access-token-expiration-time}")
    private long accessTokenExpirationTime;

    public TokenController(UserService service,
                           @Value("${jwt.secret}") String jwtSecret) {
        this.service = service;
        this.jwtSecret = jwtSecret;
    }

    @GetMapping("/refresh")
    public void refresh(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (header != null && header.startsWith("Bearer ")) {
            try {
                String refreshToken = header.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256(jwtSecret.getBytes());

                JWTVerifier verifier = JWT
                        .require(algorithm)
                        .build();

                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String username = decodedJWT.getSubject();

                User user = service.loadUserByUsername(username);
                List<String> authorities = user
                        .getAuthorities()
                        .stream()
                        .map(Authority::getAuthority)
                        .toList();

                String accessToken = JWT
                        .create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + accessTokenExpirationTime))
                        .withIssuer(request.getRequestURI())
                        .withClaim("authorities", authorities)
                        .sign(algorithm);

                response.setHeader("access_token", accessToken);
                response.setHeader("refresh_token", refreshToken);
            } catch (Exception exception) {
                ApiError apiError = new ApiError(LocalDateTime.now(), exception.getMessage(), HttpStatus.FORBIDDEN.value());

                response.setStatus(HttpStatus.FORBIDDEN.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), apiError);
            }
        }
    }
}
