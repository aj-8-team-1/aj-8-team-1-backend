package application.controllers.user;

import application.DTO.managers.CreateManagerDTO;
import application.models.user.User;
import application.services.user.ManagerService;
import application.services.user.UserService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/managers")
public class ManagerController {

    private final ManagerService managerService;
    private final UserService userService;

    public ManagerController(ManagerService managerService, UserService userService) {
        this.managerService = managerService;
        this.userService = userService;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> registerManager(@Valid @RequestBody CreateManagerDTO dto) {
        try {
            managerService.register(dto);
            return ResponseEntity.ok("Менеджер был создан");
        } catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(exception.getMessage());
        }
    }

    @GetMapping("/info")
    public String userData(Principal principal) {
        return principal.getName();
    }

    @PostMapping("/users/profile/detail/confirm")
    public ResponseEntity<String> isCompanyConfirmed(String username) {
        User user = userService.loadUserByUsername(username);
        userService.isCompanyConfirmed(user);
        return ResponseEntity.ok("Компания пользователя подтверждена");
    }
}
