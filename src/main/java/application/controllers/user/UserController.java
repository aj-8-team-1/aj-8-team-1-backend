package application.controllers.user;

import application.DTO.images.ImageDTO;
import application.DTO.users.CreateUserDTO;
import application.DTO.users.GetUserDTO;
import application.DTO.users.ResetPasswordRequestDTO;
import application.DTO.users.UpdateUserDTO;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.user.User;
import application.models.user.UserDetail;
import application.services.user.UserDetailService;
import application.services.user.UserService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final UserDetailService userDetailService;
    private final ImageMapper imageMapper;

    public UserController(UserService userService, UserDetailService userDetailService, ImageMapper imageMapper) {
        this.userService = userService;
        this.userDetailService = userDetailService;
        this.imageMapper = imageMapper;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> registerUser(@Valid @RequestBody CreateUserDTO userDTO) {
        userService.register(userDTO);
        return ResponseEntity.ok("Вам на почту отправлена ссылка для подтверждения вашей регистрации");

    }

    @GetMapping("/confirm")
    public ResponseEntity<?> confirm(@RequestParam String token) {
        userService.enable(token);
        return ResponseEntity.ok("Ваша регистрация успешно подтверждена!");
    }

    @PostMapping("/password/recovery")
    public ResponseEntity<?> recoveryPassword(@Valid @RequestBody ResetPasswordRequestDTO request) {
        userService.resetPassword(request.getToken(), request.getNewPassword());
        return ResponseEntity.ok("Пароль успешно сброшен");
    }

    @PostMapping("/password/reset")
    public ResponseEntity<?> passwordResetRequest(@RequestBody Map<String, String> requestBody) {
        String email = requestBody.get("email");
        userService.requestPasswordReset(email);
        return ResponseEntity.ok("Запрос на сброс пароля успешно отправлен");
    }

    @GetMapping("/{username}")
    public ResponseEntity<GetUserDTO> getUserById(@PathVariable String username) {
        User user = userService.loadUserByUsername(username);

        GetUserDTO userDTO = GetUserDTO.map(user);
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping
    public List<GetUserDTO> get() {
        return userService
                .findALl()
                .stream()
                .map(GetUserDTO::map)
                .collect(Collectors.toList());
    }

    @GetMapping("/profile")
    public ResponseEntity<GetUserDTO> getCurrentUser(@AuthenticationPrincipal String username) {
        User user = userService.loadUserByUsername(username);
        GetUserDTO userDTO = GetUserDTO.map(user);
        return ResponseEntity.ok(userDTO);
    }

    @PutMapping("/profile")
    public ResponseEntity<String> updateUser(@AuthenticationPrincipal String username,
                                             @RequestBody UpdateUserDTO updateUserDTO) {
        userService.updateUser(username, updateUserDTO);
        return ResponseEntity.ok("Пользователь успешно изменен");
    }

    @GetMapping("/profile/detail")
    public ResponseEntity<UserDetail> getUserDetail(@AuthenticationPrincipal String username) {
        User user = userService.loadUserByUsername(username);
        UserDetail userDetail = user.getUserDetail();
        System.out.println(userDetail);
        return ResponseEntity.ok(userDetail);
    }

    @PutMapping("/profile/detail")
    public ResponseEntity<String> updateUserDetail(@AuthenticationPrincipal String username,
                                                   @RequestBody UserDetail updatedUserDetail) {
        User user = userService.loadUserByUsername(username);
        userDetailService.updateUserDetail(user, updatedUserDetail);
        return ResponseEntity.ok("Детали пользователя успешно изменены");
    }

    @PostMapping("/profile/detail/image")
    public void uploadImage(@AuthenticationPrincipal String username,
                            @Validated @ModelAttribute ImageDTO imageDto) {
        User user = userService.loadUserByUsername(username);

        Image image = imageMapper.toEntity(imageDto);
        userService.uploadImage(user, image);
    }
}