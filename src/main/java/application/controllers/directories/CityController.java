package application.controllers.directories;

import application.exceptions.CountryNotFoundException;
import application.models.event.City;
import application.services.event.CityService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cities")
@AllArgsConstructor
public class CityController {

    private final CityService cityService;

    @GetMapping()
    public ResponseEntity<List<City>> getAll() {
        List<City> cities = cityService.getAll();
        return ResponseEntity.ok(cities);
    }

    @GetMapping("/{countryName}")
    public ResponseEntity<List<String>> getCitiesByCountry(@PathVariable String countryName) {
        List<String> cities = cityService.getCitiesByCountry(countryName);
        if (!cities.isEmpty()) {
            return ResponseEntity.ok(cities);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<String> createCity(@RequestParam String countryName, @RequestParam String cityName) {
        try {
            cityService.createCity(countryName, cityName);
            return ResponseEntity.status(HttpStatus.CREATED).body("Город успешно добавлен");
        } catch (CountryNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Страна не найдена");
        }
    }

    @DeleteMapping("/{cityName}")
    public ResponseEntity<String> deleteCityByTitle(@PathVariable String cityName) {
        cityService.deleteCity(cityName);
        return ResponseEntity.ok("Город успешно удален");
    }
}
