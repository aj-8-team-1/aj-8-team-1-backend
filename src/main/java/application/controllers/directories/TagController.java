package application.controllers.directories;


import application.models.event.Tag;
import application.services.event.TagService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")
public class TagController {
    private final TagService service;

    public TagController(TagService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<?> create(@Valid @RequestBody Tag tag) {
        service.createTag(tag);
        return ResponseEntity.ok("Тэг был создан");
    }

    @GetMapping()
    public ResponseEntity<List<Tag>> getAll() {
        List<Tag> tags = service.getAll();
        return ResponseEntity.ok(tags);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody Tag tag) {
        service.updateByIdTag(id, tag);
        return ResponseEntity.ok("Тэг был успешно обновлен");
    }

    @GetMapping("/active")
    public ResponseEntity<List<Tag>> getActiveTags() {
        List<Tag> active = service.getActive();
        return ResponseEntity.ok(active);
    }
}
