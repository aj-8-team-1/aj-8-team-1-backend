package application.controllers.directories;

import application.models.event.Organizer;
import application.services.event.OrganizerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/organizers")
@AllArgsConstructor
public class OrganizerController {
    private final OrganizerService organizerService;

    @GetMapping()
    public ResponseEntity<List<Organizer>> getAll() {
        List<Organizer> organizers = organizerService.getAll();
        return ResponseEntity.ok(organizers);
    }
}
