package application.controllers.directories;


import application.models.event.EventCategory;
import application.services.event.EventCategoryService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    private final EventCategoryService service;

    public CategoryController(EventCategoryService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<?> create(@Valid @RequestBody EventCategory category) {
        service.createEventCategory(category);
        return ResponseEntity.ok("Категония была создана");
    }

    @GetMapping()
    public ResponseEntity<List<EventCategory>> getAll() {
        List<EventCategory> categories = service.getAll();
        return ResponseEntity.ok(categories);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody EventCategory category) {
        service.updateByIdEventCategory(category, id);
        return ResponseEntity.ok("Категория была успешно обновлена");
    }

    @GetMapping("/active")
    public ResponseEntity<List<EventCategory>> getActiveCategories() {
        List<EventCategory> activeCategories = service.getActive();
        return ResponseEntity.ok(activeCategories);
    }
}
