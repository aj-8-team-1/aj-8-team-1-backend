package application.controllers.directories;


import application.DTO.images.ImageDTO;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.event.City;
import application.models.event.Place;
import application.placesSearch.PlaceSearchCriteria;
import application.services.event.CityService;
import application.services.event.PlaceService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/places")
@RequiredArgsConstructor
public class PlaceController {
    private final PlaceService placeService;
    private final ImageMapper imageMapper;
    private final CityService cityService;

    @PostMapping()
    public ResponseEntity<?> create(@RequestParam String title,
                                    @RequestParam String square,
                                    @RequestParam String capacity,
                                    @RequestParam String aspectRatio,
                                    @RequestParam String guests,
                                    @RequestParam String rate,
                                    @RequestParam String address,
                                    @RequestParam Long id,
                                    @Validated @ModelAttribute ImageDTO imageDto) {
        Image image = imageMapper.toEntity(imageDto);
        City city = cityService.getByIdCity(id);

        placeService.createPlaceWithImage(title, square, capacity, aspectRatio, guests, rate, address, image, city);
        return ResponseEntity.ok("Площадка была создана");
    }

    @GetMapping()
    public ResponseEntity<List<Place>> getAll() {
        List<Place> places = placeService.getAll();
        return ResponseEntity.ok(places);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody Place updatedPlace) {
        placeService.updatePlace(id, updatedPlace);
        return ResponseEntity.ok("Площадка была успешно обновлена");
    }

    @GetMapping("/active")
    public ResponseEntity<List<Place>> getActivePlaces() {
        List<Place> active = placeService.getActive();
        return ResponseEntity.ok(active);
    }

    @PostMapping("/{id}/image")
    public void uploadImage(@PathVariable long id,
                            @Validated @ModelAttribute ImageDTO imageDto) {
        Image image = imageMapper.toEntity(imageDto);
        placeService.uploadImage(id, image);
    }

    @GetMapping("/city/{id}")
    public ResponseEntity<List<Place>> getPlacesByCity(@PathVariable Long id){
        City city = cityService.getByIdCity(id);
        List<Place> places = placeService.findPlaceByCity(city);
        return ResponseEntity.ok(places);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Place>> searchPlaces(@RequestBody PlaceSearchCriteria placeCriteria) {
        List<Place> places = placeService.searchPlaces(placeCriteria);
        return ResponseEntity.ok(places);
    }
}
