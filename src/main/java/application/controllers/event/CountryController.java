package application.controllers.event;

import application.services.event.CountryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/countries")
@AllArgsConstructor
public class CountryController {
    private final CountryService countryService;

    @GetMapping("/grouped")
    public ResponseEntity<Map<String, List<String>>> getGroupedCities() {
        Map<String, List<String>> groupedCities = countryService.getGroupedCities();
        return ResponseEntity.ok(groupedCities);
    }

    @PostMapping
    public ResponseEntity<String> createCountry(@RequestParam String countryName) {
        countryService.createCountry(countryName);
        return ResponseEntity.status(HttpStatus.CREATED).body("Страна успешно добавлена");
    }

    @DeleteMapping("/{countryName}")
    public ResponseEntity<String> deleteCountryByTitle(@PathVariable String countryName) {
        countryService.deleteCountry(countryName);
        return ResponseEntity.ok("Страна успешно удалена");
    }
}
