package application.controllers.event;

import application.DTO.events.EventDTO;
import application.DTO.images.ImageDTO;
import application.models.event.Event;
import application.models.event.EventRequest;
import application.models.event.EventRequestStatus;
import application.services.event.EventRequestService;
import application.services.event.EventService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manager/events")
public class ManagerEventController {

    private final EventService eventService;
    private final EventRequestService requestService;

    public ManagerEventController(EventService eventService, EventRequestService requestService) {
        this.eventService = eventService;
        this.requestService = requestService;
    }

    @GetMapping
    public ResponseEntity<Page<Event>> getAllEvents(@PageableDefault(size = 20) Pageable pageable) {
        Page<Event> events = eventService.getAllEvents(pageable);
        return ResponseEntity.ok(events);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Event> getEventById(@PathVariable Long id) {
        Event event = eventService.getEventById(id);
        return ResponseEntity.ok(event);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createEvent(@Valid @RequestBody EventDTO eventDTO) {
        Long id = eventService.createEvent(eventDTO);
        return ResponseEntity.ok(id);
    }

    @PostMapping("/create/images/{id}")
    public ResponseEntity<?> createImagesByEvent(@PathVariable Long id, @Validated @ModelAttribute ImageDTO imageDTO) {
        eventService.createEventImages(imageDTO, id);
        return ResponseEntity.ok("Картинки успешно добавлены");
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateEventById(@Valid @RequestBody EventDTO event, @PathVariable Long id) {
        eventService.updateEventById(event, id);
        return ResponseEntity.ok("Событие успешно обновлено");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEventById(@PathVariable Long id) {
        eventService.deleteEventById(id);
        return ResponseEntity.ok("Событие успешно удалено");
    }

    @GetMapping("/requests")
    public ResponseEntity<Page<EventRequest>> getRequests(@PageableDefault(size = 20) Pageable pageable) {
        Page<EventRequest> events = requestService.getAllEvents(pageable);
        return ResponseEntity.ok(events);
    }

    @GetMapping("/requests/new")
    public ResponseEntity<Page<EventRequest>> getNewEventRequests(@PageableDefault(size = 20) Pageable pageable) {
        Page<EventRequest> eventRequests = requestService.getAllEventsByStatus(EventRequestStatus.NEW, pageable);
        return ResponseEntity.ok(eventRequests);
    }

    @GetMapping("/requests/process")
    public ResponseEntity<Page<EventRequest>> getProcessEventRequests(@PageableDefault(size = 20) Pageable pageable) {
        Page<EventRequest> eventRequests = requestService.getAllEventsByStatus(EventRequestStatus.PROCESS, pageable);
        return ResponseEntity.ok(eventRequests);
    }

    @PostMapping("/requests/{id}/start")
    public ResponseEntity<String> startEventProcessing(@PathVariable Long id, @AuthenticationPrincipal String username) {
        requestService.startProcessing(id, username);
        return ResponseEntity.ok("Обработка события начата успешно");
    }

    @PostMapping("/requests/{id}/approve")
    public ResponseEntity<String> approveEventProcessing(@PathVariable Long id) {
        requestService.approveProcessing(id);
        return ResponseEntity.ok("Обработка события успешно завершена");
    }

    @DeleteMapping("/requests/{id}/reject")
    public ResponseEntity<String> rejectEventProcessing(@PathVariable Long id) {
        requestService.rejectProcessing(id);
        return ResponseEntity.ok("Обработка события успешно завершена");
    }

    @GetMapping("/requests/{id}")
    public ResponseEntity<EventRequest> getEventRequestById(@PathVariable Long id) {
        EventRequest eventRequest = requestService.getEventById(id);
        return ResponseEntity.ok(eventRequest);
    }

    @PutMapping("/addRecommendation/{id}")
    public ResponseEntity<String> addRecommendationInEvent(@PathVariable Long id) {
        eventService.addRecommendation(id);
        return ResponseEntity.ok("Ивент успешно добавлен в рекомендации");
    }

    @PutMapping("/retractRecommendation/{id}")
    public ResponseEntity<String> retractRecommendationInEvent(@PathVariable Long id) {
        eventService.retractRecommendation(id);
        return ResponseEntity.ok("Рекомендация ивента успешно отозвана");
    }
}
