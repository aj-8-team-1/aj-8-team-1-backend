package application.controllers.event;

import application.DTO.events.EventRequestDTO;
import application.DTO.images.ImageDTO;
import application.eventsSearch.EventSearchCriteria;
import application.models.event.City;
import application.models.event.Event;
import application.models.event.EventCategory;
import application.services.event.CityService;
import application.services.event.EventCategoryService;
import application.services.event.EventRequestService;
import application.services.event.EventService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
public class UserEventController {

    private final EventService eventService;
    private final EventRequestService eventRequestService;
    private final EventCategoryService eventCategoryService;
    private final CityService cityService;

    public UserEventController(EventService eventService, EventRequestService eventRequestService, EventCategoryService eventCategoryService, CityService cityService) {
        this.eventService = eventService;
        this.eventRequestService = eventRequestService;
        this.eventCategoryService = eventCategoryService;
        this.cityService = cityService;
    }

    @GetMapping
    public ResponseEntity<Page<Event>> getAllEvents(@PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(eventService.getAllEvents(pageable));
    }


    @GetMapping("/{id}")
    public ResponseEntity<Event> getEventById(@PathVariable Long id) {
        Event event = eventService.getEventById(id);
        return ResponseEntity.ok(event);
    }

    @PostMapping("/create")
    public ResponseEntity<?> sendRequest(@RequestBody EventRequestDTO dto, @AuthenticationPrincipal String username) {
        Long id = eventRequestService.createEvent(dto, username);
        return ResponseEntity.ok(id);
    }

    @PostMapping("/create/images/{id}")
    public ResponseEntity<?> createImagesByEvent(@PathVariable Long id, @Validated @ModelAttribute ImageDTO imageDTO) {
        eventRequestService.createEventRequestImages(imageDTO, id);
        return ResponseEntity.ok("Картинки успешно добавлены");
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<Event>> getEventsByCategory(@PathVariable Long id) {
        EventCategory category = eventCategoryService.getByIdEventCategory(id);

        List<Event> events = eventService.getEventsByCategory(category);
        return ResponseEntity.ok(events);
    }
    @GetMapping("byFormat/{format}")
    public ResponseEntity<List<Event>> getEventsByFormat(@PathVariable String format){
        List<Event> events = eventService.getEventsByFormat(format);
        return ResponseEntity.ok(events);
    }
    @GetMapping("city/{id}")
    public ResponseEntity<List<Event>> getEventsByCity(@PathVariable Long id){
        City city = cityService.getByIdCity(id);
        List<Event> events = eventService.getEventsByCity(city);
        return ResponseEntity.ok(events);
    }
    @GetMapping("/filter")
    public ResponseEntity<List<Event>> getEventsByFormatAndCity(@RequestParam String format, @RequestParam Long id) {
        City city = cityService.getByIdCity(id);
        List<Event> events = eventService.getEventsByFormatAndCity(format, city);
        return ResponseEntity.ok(events);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Event>> searchEvents(@RequestBody EventSearchCriteria eventCriteria) {
        List<Event> events = eventService.searchEvents(eventCriteria);
        return ResponseEntity.ok(events);
    }

}
