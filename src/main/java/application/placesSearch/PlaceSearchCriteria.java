package application.placesSearch;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlaceSearchCriteria {
    private String title;
    private String address;
}

