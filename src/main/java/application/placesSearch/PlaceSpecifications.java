package application.placesSearch;

import application.models.event.Place;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.criteria.Predicate;

public class PlaceSpecifications {
    public static Specification<Place> titleLike(String title) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("title"), "%" + title + "%");
    }

    public static Specification<Place> addressLike(String address) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("address"), "%" + address + "%");
    }

    public static Specification<Place> buildSpecification(PlaceSearchCriteria placeCriteria) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (placeCriteria.getTitle() != null) {
                predicates.add(titleLike(placeCriteria.getTitle()).toPredicate(root, query, criteriaBuilder));
            }
            if (placeCriteria.getAddress() != null) {
                predicates.add(addressLike(placeCriteria.getAddress()).toPredicate(root, query, criteriaBuilder));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}

