package application.constants;

import lombok.Getter;

@Getter
public enum ApplicationConstants {
    REGISTRATION_CONFIRMATION_URL("http://localhost:5173/users/registration_approve?token="),
    REGISTRATION_EMAIL_TEMPLATE_PATH("src/main/resources/templates/registration-email-template.html"),

    RESET_PASSWORD_CONFIRMATION_URL("http://localhost:5173/users/reset-password?token="),
    RESET_PASSWORD_TEMPLATE_PATH("src/main/resources/templates/reset-password-email-template.html"),

    EVENT_REQUEST_IN_PROCESS_TEMPLATE_PATH("src/main/resources/templates/event-request-in-process.html"),

    EVENT_REQUEST_IS_CANCELED_TEMPLATE_PATH("src/main/resources/templates/event-request-is-canceled.html"),

    EVENT_REQUEST_IS_APPROVED_URL("http://localhost:5173/events/"),
    EVENT_REQUEST_IS_APPROVED_TEMPLATE_PATH("src/main/resources/templates/event-request-is-approved.html");

    private final String value;

    ApplicationConstants(String value) {
        this.value = value;
    }

}