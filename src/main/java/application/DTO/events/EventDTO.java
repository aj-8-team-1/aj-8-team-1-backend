package application.DTO.events;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class EventDTO {
    @NotBlank(message = "Title cannot be blank")
    @Size(max = 50, message = "Title cannot exceed 50 characters")
    private String title;

    @NotNull(message = "Start date cannot be null")
    private LocalDateTime startDate;

    @Size(max = 250, message = "Description cannot exceed 250 characters")
    private String description;

    @NotNull(message = "End date cannot be null")
    private LocalDateTime endDate;

    @NotBlank(message = "Format cannot be blank")
    @Size(max = 30, message = "Format cannot exceed 30 characters")
    private String format;

    @NotNull(message = "City ID cannot be null")
    private Long cityIds;

    @NotNull(message = "Organizer ID cannot be null")
    private Long organizerIds;

    @NotNull(message = "Website cannot be null")
    private String website;

    @NotNull(message = "Event category ID cannot be null")
    private Long categoryIds;

    @NotNull(message = "Идентификаторы тегов не могут быть пустыми")
    private List<Long> tagIds;

    private List<String> images;

    @NotNull(message = "Место проведения не может быть пустым")
    private Long placeId;
}
