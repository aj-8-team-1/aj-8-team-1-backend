package application.DTO.events;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class EventRequestDTO {
    @NotBlank(message = "Заголовок не может быть пустым")
    @Size(max = 50, message = "Заголовок не может превышать 50 символов")
    private String title;

    @Size(max = 250, message = "Описание не может превышать 250 символов")
    private String description;

    @NotBlank(message = "Формат не может быть пустым")
    @Size(max = 30, message = "Формат не может превышать 30 символов")
    private String format;

    private String comment;

    @NotNull(message = "Дата начала не может быть пустой")
    private LocalDateTime startDate;

    @NotNull(message = "Дата окончания не может быть пустой")
    private LocalDateTime endDate;

    @NotNull(message = "Идентификатор города не может быть пустым")
    private Long cityIds;

    @NotNull(message = "Идентификатор организатора не может быть пустым")
    private Long organizerId;

    private List<String> websites;

    @NotNull(message = "Идентификатор категории события не может быть пустым")
    private Long categoryIds;

    @NotNull(message = "Идентификаторы тегов не могут быть пустыми")
    private List<Long> tagIds;

    private List<String> images;

    @NotNull(message = "Место проведения не может быть пустым")
    private Long placeId;
}
