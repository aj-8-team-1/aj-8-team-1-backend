package application.DTO.events;

import application.models.event.Event;
import application.models.event.Tag;
import application.models.event.Website;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class GetEventDTO {
    @NotBlank(message = "Title cannot be blank")
    @Size(max = 50, message = "Title cannot exceed 50 characters")
    private String title;

    @NotNull(message = "Start date cannot be null")
    private LocalDateTime startDate;

    @Size(max = 250, message = "Description cannot exceed 250 characters")
    private String description;

    private LocalDateTime endDate;

    @NotBlank(message = "Format cannot be blank")
    @Size(max = 30, message = "Format cannot exceed 30 characters")
    private String format;

    @NotNull(message = "City ID cannot be null")
    private Long cityId;

    @NotNull(message = "Organizer ID cannot be null")
    private Long organizerId;

    @NotNull(message = "Website IDs cannot be null")
    private List<Long> websiteIds;

    @NotNull(message = "Event category ID cannot be null")
    private Long eventCategoryId;

    @NotNull(message = "Tag IDs cannot be null")
    private List<Long> tagIds;

    @NotNull(message = "Photo IDs cannot be null")
    private List<String> images;

    @NotNull(message = "Place IDs cannot be null")
    private Long placeId;

    public static GetEventDTO map(Event event) {
        List<Long> webSiteIds = new ArrayList<>();
        for (Website website : event.getWebsites()) {
            webSiteIds.add(website.getId());
        }

        List<Long> tagIds = new ArrayList<>();
        for (Tag tag : event.getTags()) {
            tagIds.add(tag.getId());
        }

        return GetEventDTO
                .builder()
                .title(event.getTitle())
                .startDate(event.getStartDate())
                .description(event.getDescription())
                .endDate(event.getEndDate())
                .format(event.getFormat())
                .cityId(event.getCity().getId())
                .organizerId(event.getOrganizer().getId())
                .websiteIds(webSiteIds)
                .eventCategoryId(event.getEventCategory().getId())
                .tagIds(tagIds)
                .images(event.getImages())
                .placeId(event.getPlace().getId())
                .build();
    }
}
