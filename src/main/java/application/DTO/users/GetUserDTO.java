package application.DTO.users;

import application.models.user.Authority;
import application.models.user.User;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDTO {
    private String email;

    private String firstName;
    private String lastName;

    private List<Authority> authorities;

    public static GetUserDTO map(User user) {
        return GetUserDTO
                .builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .authorities(user.getAuthorities())
                .build();
    }
}