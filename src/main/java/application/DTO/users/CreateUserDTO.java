package application.DTO.users;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CreateUserDTO {
    @Size(min = 3, max = 15, message = "Имя пользователя должно содержать от 3 до 15 символов")
    @NotBlank(message = "Имя пользователя обязательно")
    private String firstName;

    @Size(min = 3, max = 15, message = "Фамилия пользователя должна содержать от 3 до 15 символов")
    @NotBlank(message = "Фамилия пользователя обязательна")
    private String lastName;

    @Email(message = "Email должен быть действительным")
    @NotBlank(message = "Email не может быть пустым")
    private String email;

    @Size(min = 6, max = 15, message = "Пароль должен содержать от 6 до 15 символов")
    @NotBlank(message = "Пароль не может быть пустым")
    private String password;


    @Size(min = 3, max = 15, message = "Права должны содержать от 3 до 15 символов")
    @NotBlank(message = "Права не могут быть пустыми")
    private String authority = "USER";

    @NotBlank(message = "Провайдер не может быть пустым")
    private String provider = "LOCAL";
}