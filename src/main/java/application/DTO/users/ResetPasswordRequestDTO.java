package application.DTO.users;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ResetPasswordRequestDTO {
    private String token;

    @Size(min = 6, max = 15, message = "Пароль должен содержать от 6 до 15 символов")
    @NotBlank(message = "Пароль не может быть пустым")
    private String newPassword;
}
