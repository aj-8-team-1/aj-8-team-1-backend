package application.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@AllArgsConstructor
public class ApiError {

    private final String timestamp;
    private final String message;
    private final Integer code;

    public ApiError(LocalDateTime timestamp, String message, Integer code) {
        this.timestamp = timestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        this.message = message;
        this.code = code;
    }
}