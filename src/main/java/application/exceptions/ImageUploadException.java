package application.exceptions;

public class ImageUploadException extends RuntimeException {

    public ImageUploadException(final String message) {
        super(message);
    }

}