package application.eventsSearch;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventSearchCriteria {
    private String title;
    private String format;
}
