package application.eventsSearch;

import application.models.event.Event;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class EventSpecifications {

    public static Specification<Event> titleLike(String title) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("title"), "%" + title + "%");
    }

    public static Specification<Event> formatEquals(String format) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("format"), format);
    }

    public static Specification<Event> buildSpecification(EventSearchCriteria criteria) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (criteria.getTitle() != null) {
                predicates.add(titleLike(criteria.getTitle()).toPredicate(root, query, criteriaBuilder));
            }
            if (criteria.getFormat() != null) {
                predicates.add(formatEquals(criteria.getFormat()).toPredicate(root, query, criteriaBuilder));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
