package application.models.event;

import application.models.BaseEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "places")
public class Place extends BaseEntity {
    private String title;
    private String address;

    private boolean active;

    @Column(name = "image")
    @CollectionTable(name = "place_images")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> images = new ArrayList<>();
    @ManyToOne
    private City city;

    private String square;
    private String capacity;
    private String aspectRatio;
    private String guests;
    private String rate;
}
