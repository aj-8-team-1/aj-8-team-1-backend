package application.models.event;

import application.models.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "event_categories")
public class EventCategory extends BaseEntity {
    private String title;
    private boolean active;
}
