package application.models.event;

import application.models.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tags")
public class Tag extends BaseEntity {
    @NotBlank(message = "Title cannot be blank")
    @Size(max = 20, message = "Title cannot exceed 20 characters")
    private String title;

    private boolean active;
}
