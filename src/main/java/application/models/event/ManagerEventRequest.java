package application.models.event;

import application.models.BaseEntity;
import application.models.user.Manager;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "manager_event_request")
public class ManagerEventRequest extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Manager manager;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private EventRequest eventRequest;

    private LocalDateTime date;
}
