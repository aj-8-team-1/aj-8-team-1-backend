package application.models.event;

import application.models.BaseEntity;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "events")
public class Event extends BaseEntity {
    private String title;
    private LocalDateTime startDate;
    private String description;
    private LocalDateTime endDate;
    private String format;
    private boolean recommendation;

    @ManyToOne
    private City city;

    @ManyToOne
    private Organizer organizer;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "event_websites",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "website_id"))
    private List<Website> websites;

    @ManyToOne
    private EventCategory eventCategory;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "event_tags",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @Column(name = "image")
    @CollectionTable(name = "event_images")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> images = new ArrayList<>();

    @ManyToOne
    private Place place;
}
