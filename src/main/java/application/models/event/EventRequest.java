package application.models.event;

import application.models.BaseEntity;
import application.models.user.Manager;
import application.models.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "event_requests")
public class EventRequest extends BaseEntity {
    private String title;
    private String description;
    private String format;
    private String comment;

    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @Enumerated(EnumType.STRING)
    private EventRequestStatus status;

    @OneToOne
    private City city;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Website> websites;

    @ManyToOne
    private EventCategory eventCategory;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "event_request_tags",
            joinColumns = @JoinColumn(name = "event_request_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @Column(name = "image")
    @CollectionTable(name = "event_request_images")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> images = new ArrayList<>();

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Manager manager;

    @ManyToOne
    private Place place;

    @ManyToOne
    private User user;
}
