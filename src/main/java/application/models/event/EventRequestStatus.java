package application.models.event;

public enum EventRequestStatus {
    NEW,
    PROCESS,
    FINISH
}
