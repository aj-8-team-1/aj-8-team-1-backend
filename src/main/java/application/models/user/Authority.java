package application.models.user;

import application.models.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "authorities")
public class Authority extends BaseEntity implements GrantedAuthority {

    @Size(min = 3, max = 15, message = "Права должны содержать от 3 до 10 символов")
    private String authority;
}