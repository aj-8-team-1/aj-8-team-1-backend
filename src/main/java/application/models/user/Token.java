package application.models.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Token {

    private static final int EXPIRATION = 60 * 3;
    private Long userID;
    private String token;
    private Date expire;

    public Token(Long userID) {
        this.userID = userID;

        this.token = UUID.randomUUID().toString();
        this.expire = calculate();
    }

    private Date calculate() {
        Calendar calendar = Calendar.getInstance();

        long time = calendar
                .getTime()
                .getTime();

        calendar.setTime(new Timestamp(time));
        calendar.add(Calendar.MINUTE, EXPIRATION);

        return calendar.getTime();
    }
}
