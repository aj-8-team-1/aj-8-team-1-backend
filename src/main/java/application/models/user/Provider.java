package application.models.user;

public enum Provider {
    LOCAL, GOOGLE
}