package application.models.user;

import application.models.BaseEntity;
import application.models.event.City;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_details")
public class UserDetail extends BaseEntity {
    private int age;
    private String gender;
    private String phoneNumber;
    private String position;
    private String company;
    private boolean companyConfirmation;

    @OneToOne
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    private City city;

    @Column(name = "image")
    private String image;
}