package application.eventsMail;

import application.models.event.EventRequest;
import application.models.user.Manager;
import application.models.user.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class EmailEvent extends ApplicationEvent {

    public enum EventType {
        REGISTRATION_COMPLETE,
        RESET_PASSWORD,
        REQUEST_PROCESSING,
        REQUEST_APPROVE,
        REQUEST_REJECT
    }

    private User user;
    private EventType eventType;
    private String templatePath;
    private Manager manager;
    private Long eventId;
    private EventRequest eventRequest;

    public EmailEvent(User user, EventType eventType, String templatePath) {
        super(user);
        this.user = user;
        this.eventType = eventType;
        this.templatePath = templatePath;
    }

    public EmailEvent(User user, EventType eventType, String templatePath, Long eventId) {
        super(user);
        this.user = user;
        this.eventType = eventType;
        this.templatePath = templatePath;
        this.eventId = eventId;
    }

    public EmailEvent(User user, EventType eventType, String templatePath, EventRequest eventRequest) {
        super(user);
        this.user = user;
        this.eventType = eventType;
        this.templatePath = templatePath;
        this.eventRequest = eventRequest;
    }

    public EmailEvent(Manager manager, EventType eventType, String templatePath) {
        super(manager);
        this.manager = manager;
        this.eventType = eventType;
        this.templatePath = templatePath;
    }
}
