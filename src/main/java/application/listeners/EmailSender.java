package application.listeners;

import application.constants.ApplicationConstants;
import application.eventsMail.EmailEvent;
import application.models.user.Token;
import application.models.user.User;
import application.services.user.TokenService;
import jakarta.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@AllArgsConstructor
public class EmailSender {

    private final JavaMailSender sender;
    private final TokenService service;

    public void sendEmail(EmailEvent event) {
        User user = event.getUser();
        Token token = new Token(user.getId());

        String username = user.getEmail();
        String subject = "";
        String url = "";
        String teg = "";

        switch (event.getEventType()) {
            case REGISTRATION_COMPLETE:
                subject = "Подтверждение регистрации";
                url = ApplicationConstants.REGISTRATION_CONFIRMATION_URL.getValue() + token.getToken();
                teg = "emails:";
                break;
            case RESET_PASSWORD:
                subject = "Сброс пароля";
                url = ApplicationConstants.RESET_PASSWORD_CONFIRMATION_URL.getValue() + token.getToken();
                teg = "passwordReset:";
                break;
            case REQUEST_APPROVE:
                subject = "Запрос ивента принят";
                url = ApplicationConstants.EVENT_REQUEST_IS_APPROVED_URL.getValue() + event.getEventId();
                break;
            case REQUEST_PROCESSING:
                subject = "Запрос ивента в обработке";
                break;
            case REQUEST_REJECT:
                subject = "Запрос ивента отклонен";
                break;
        }

        service.set(token, teg);

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, "utf-8");

        try {
            helper.setTo(username);
            helper.setSubject(subject);

            String htmlContent = readHtmlContentFromFile(event.getTemplatePath());

            htmlContent = htmlContent.replace("[имя пользователя]", user.getFirstName() + " " + user.getLastName());
            htmlContent = htmlContent.replace("[URL]", url);

            helper.setText(htmlContent, true);

            sender.send(message);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String readHtmlContentFromFile(String templatePath) throws IOException {
        return Files.readString(Paths.get(templatePath));
    }
}
