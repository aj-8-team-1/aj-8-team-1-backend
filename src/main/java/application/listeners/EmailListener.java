package application.listeners;

import application.eventsMail.EmailEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class EmailListener implements ApplicationListener<EmailEvent> {

    private final EmailSender emailSender;

    @Override
    public void onApplicationEvent(@NonNull EmailEvent event) {
        emailSender.sendEmail(event);
    }
}