package application.configurations;

import application.authenticationManager.impl.ManagerAuthentication;
import application.authenticationManager.impl.UserAuthentication;
import application.filters.impl.AuthorizationFilterImpl;
import application.filters.impl.CustomOAuth2AccessTokenResponseClient;
import application.filters.impl.ManagerAuthenticationFilterImpl;
import application.filters.impl.UserAuthenticationFilterImpl;
import application.models.user.CustomOAuth2User;
import application.services.impl.user.CustomOAuth2UserService;
import application.services.user.ManagerService;
import application.services.user.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.endpoint.DefaultMapOAuth2AccessTokenResponseConverter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfiguration {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.access-token-expiration-time}")
    private long accessTokenExpirationTime;

    @Value("${jwt.refresh-token-expiration-time}")
    private long refreshTokenExpirationTime;
    private final UserService userService;
    private final ManagerService managerService;
    private final PasswordEncoder encoder;
    private final AuthorizationFilterImpl authorizationFilter;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final CustomOAuth2UserService oAuth2UserService;

    public SecurityConfiguration(UserService userService,
                                 ManagerService managerService,
                                 PasswordEncoder encoder,
                                 AuthorizationFilterImpl authorizationFilter,
                                 AuthenticationManagerBuilder authenticationManagerBuilder,
                                 CustomOAuth2UserService oAuth2UserService) {
        this.userService = userService;
        this.managerService = managerService;
        this.encoder = encoder;
        this.authorizationFilter = authorizationFilter;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.oAuth2UserService = oAuth2UserService;
    }

    @Bean
    public SecurityFilterChain filters(HttpSecurity security) throws Exception {
        UserAuthenticationFilterImpl userFilter = new UserAuthenticationFilterImpl(userAuthentication(), jwtSecret, accessTokenExpirationTime, refreshTokenExpirationTime);
        ManagerAuthenticationFilterImpl managerFilter = new ManagerAuthenticationFilterImpl(managerAuthentication(), jwtSecret, accessTokenExpirationTime, refreshTokenExpirationTime);
        userFilter.setFilterProcessesUrl("/users/login");
        managerFilter.setFilterProcessesUrl("/managers/login");

        security
                .authorizeHttpRequests
                        (
                                (authorize) -> authorize
                                        .requestMatchers("/managers/registration").permitAll()
                                        .requestMatchers("/managers/login").permitAll()
                                        .requestMatchers("/managers/**").hasAuthority("MANAGER")
                                        .requestMatchers("/manager/events").hasAuthority("MANAGER")

                                        .requestMatchers("/users/registration").permitAll()
                                        .requestMatchers("/users/login").permitAll()
                                        .anyRequest().permitAll()
                        )
                .cors
                        (
                                (cors) ->
                                {
                                    CorsConfiguration configuration = getCorsConfiguration();

                                    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
                                    source.registerCorsConfiguration("/**", configuration);

                                    cors.configurationSource(source);
                                }
                        )
                .oauth2Login(login -> login
                        .userInfoEndpoint(userInfo -> userInfo.userService(oAuth2UserService))
                        .successHandler((request, response, authentication) -> {

                            OAuth2User oauth2User = (OAuth2User) authentication.getPrincipal();
                            CustomOAuth2User customOAuth2User = new CustomOAuth2User(oauth2User);
                            List<String> authorities = customOAuth2User
                                    .getAuthorities()
                                    .stream()
                                    .map(GrantedAuthority::getAuthority)
                                    .collect(Collectors.toList());

                            Algorithm algorithm = Algorithm.HMAC256(jwtSecret.getBytes());

                            String accessToken = JWT
                                    .create()
                                    .withSubject(customOAuth2User.getName())
                                    .withExpiresAt(new Date(System.currentTimeMillis() + accessTokenExpirationTime))
                                    .withIssuer(request.getRequestURI())
                                    .withClaim("authorities", authorities)
                                    .sign(algorithm);

                            String refreshToken = JWT
                                    .create()
                                    .withSubject(customOAuth2User.getName())
                                    .withExpiresAt(new Date(System.currentTimeMillis() + refreshTokenExpirationTime))
                                    .withIssuer(request.getRequestURI())
                                    .sign(algorithm);
                            userService.processOAuthPostLogin(customOAuth2User);

                            response.sendRedirect("http://localhost:8080/api/v1/users/home");
                            response.setHeader("access_token", accessToken);
                            response.setHeader("refresh_token", refreshToken);
                        })
                        .tokenEndpoint(token -> {
                            var defaultMapConverter = new DefaultMapOAuth2AccessTokenResponseConverter();
                            var httpConverter = getoAuth2AccessTokenResponseHttpMessageConverter(defaultMapConverter);

                            var restOperations = new RestTemplate(List.of(new FormHttpMessageConverter(), httpConverter));
                            restOperations.setErrorHandler(new OAuth2ErrorResponseErrorHandler());

                            var client = new CustomOAuth2AccessTokenResponseClient(restOperations);
                            token.accessTokenResponseClient(client);
                        })
                )
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement((sessionManagement) -> sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilter(userFilter)
                .addFilter(managerFilter)
                .addFilterBefore(authorizationFilter, UsernamePasswordAuthenticationFilter.class);

        return security.build();
    }

    @NotNull
    private static OAuth2AccessTokenResponseHttpMessageConverter getoAuth2AccessTokenResponseHttpMessageConverter(DefaultMapOAuth2AccessTokenResponseConverter defaultMapConverter) {
        Converter<Map<String, Object>, OAuth2AccessTokenResponse> customMapConverter = tokenResponse -> {
            var withTokenType = new HashMap<>(tokenResponse);
            withTokenType.put(OAuth2ParameterNames.TOKEN_TYPE, OAuth2AccessToken.TokenType.BEARER.getValue());
            return  defaultMapConverter.convert(withTokenType);
        };

        var httpConverter = new OAuth2AccessTokenResponseHttpMessageConverter();
        httpConverter.setAccessTokenResponseConverter(customMapConverter);
        return httpConverter;
    }

    public void configureGlobal() {
        authenticationManagerBuilder.authenticationProvider(userServiceProvider());
        authenticationManagerBuilder.authenticationProvider(managerServiceProvider());
    }

    private static CorsConfiguration getCorsConfiguration() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedHeader("*");

        configuration.addAllowedMethod("GET");
        configuration.addAllowedMethod("POST");
        configuration.addAllowedMethod("PUT");
        configuration.addAllowedMethod("PATCH");
        configuration.addAllowedMethod("DELETE");

        configuration.addAllowedOrigin("http://localhost:63342");
        configuration.addAllowedOrigin("http://localhost:63343");
        configuration.addAllowedOrigin("http://localhost:5173");
        configuration.addAllowedOrigin("http://localhost:5174");
        configuration.addAllowedOrigin("http://localhost:5176");
        configuration.addAllowedOrigin("http://localhost:5175");
        configuration.addAllowedOrigin("http://localhost:443");
        configuration.addAllowedOrigin("http://localhost");
        configuration.setAllowCredentials(true);

        configuration.setExposedHeaders(Arrays.asList("access_token", "refresh_token"));
        return configuration;
    }

    @Bean
    public DaoAuthenticationProvider userServiceProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(userService);
        provider.setPasswordEncoder(encoder);

        return provider;
    }

    @Bean
    public DaoAuthenticationProvider managerServiceProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(managerService);
        provider.setPasswordEncoder(encoder);

        return provider;
    }

    @Bean
    public UserAuthenticationFilterImpl userAuthenticationFilter() throws Exception {
        return new UserAuthenticationFilterImpl(userAuthentication(), jwtSecret, accessTokenExpirationTime, refreshTokenExpirationTime);
    }

    @Bean
    public ManagerAuthenticationFilterImpl managerAuthenticationFilter() throws Exception {
        return new ManagerAuthenticationFilterImpl(managerAuthentication(), jwtSecret, accessTokenExpirationTime, refreshTokenExpirationTime);
    }

    @Bean
    @Primary
    public UserAuthentication userAuthentication() {
        return new UserAuthentication(this.userService, this.encoder);
    }

    @Bean
    public ManagerAuthentication managerAuthentication() {
        return new ManagerAuthentication(this.managerService, this.encoder);
    }
}