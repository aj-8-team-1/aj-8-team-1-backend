package application.repositories.event;

import application.models.event.Organizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizerRepository extends JpaRepository<Organizer, Long> {

    Organizer getOrganizerById(Long id);

    Organizer findByTitleIgnoreCase(String title);
}
