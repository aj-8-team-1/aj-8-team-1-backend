package application.repositories.event;

import application.models.event.City;
import application.models.event.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long>, JpaSpecificationExecutor<Place> {
    Optional<Place> findByTitle(String title);

    List<Place> findByActiveTrue();
    List<Place> findByCity(City city);
}
