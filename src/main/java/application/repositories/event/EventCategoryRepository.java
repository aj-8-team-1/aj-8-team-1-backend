package application.repositories.event;

import application.models.event.EventCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventCategoryRepository extends JpaRepository<EventCategory, Long> {

    EventCategory getEventCategoryByTitleIgnoreCase(String title);

    List<EventCategory> findByActiveTrue();
}
