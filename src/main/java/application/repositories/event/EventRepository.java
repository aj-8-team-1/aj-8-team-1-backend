package application.repositories.event;

import application.models.event.City;
import application.models.event.Event;
import application.models.event.EventCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event> {
    List<Event> findEventByEventCategory(EventCategory eventCategory);
    List<Event> findByCity(City city);
    List<Event> findByFormatAndCity(String format, City city);
    List<Event> findByFormat(String format);
}
