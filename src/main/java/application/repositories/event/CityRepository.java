package application.repositories.event;

import application.models.event.City;
import application.models.event.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    List<City> findByCountry(Country country);

    Optional<City> findByTitle(String title);

    City getCityByTitleIgnoreCase(String title);
}
