package application.repositories.event;

import application.models.event.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Tag getTagByTitleIgnoreCase(String title);

    List<Tag> findByActiveTrue();
}
