package application.repositories.event;

import application.models.event.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Optional<Country> findByTitle(String title);

    Country getCountriesByTitle(String title);
}
