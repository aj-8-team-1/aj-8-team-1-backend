package application.services.event;

import application.models.event.Tag;

import java.util.List;

public interface TagService {
    Tag getByIdTag(Long id);

    List<Tag> getAll();

    List<Tag> getActive();

    void createTag(Tag tag);

    void updateByIdTag(Long id, Tag updatedTag);

    void deleteByIdTag(Long id);

    List<Tag> getTagList(List<Long> tagIds);

    List<String> getNewTags(String tags);

    Tag getTagByTitle(String title);
}
