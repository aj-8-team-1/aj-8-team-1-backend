package application.services.event;

import application.models.event.Organizer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrganizerService {
    Page<Organizer> getAllOrganizers(Pageable pageable);

    List<Organizer> getAll();

    Organizer getByIdOrganizer(Long id);

    void createOrganizer(Organizer organizer);

    void deleteByIdOrganizer(Long id);

    void updateByIdOrganizer(Organizer organizer, Long id);

    void saveOrganizer(Organizer organizer);

    Organizer getByTitle(String title);
}
