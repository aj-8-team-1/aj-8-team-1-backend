package application.services.event;

import application.DTO.events.EventRequestDTO;
import application.DTO.images.ImageDTO;
import application.models.event.EventRequest;
import application.models.event.EventRequestStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EventRequestService {
    Page<EventRequest> getAllEvents(Pageable pageable);

    EventRequest getEventById(Long id);

    Long createEvent(EventRequestDTO event, String username);

    void createEventRequestImages(ImageDTO imageDTO, Long eventId);

    void deleteEventById(Long eventId);

    void deleteEventRequest(Long id);

    void updateEventById(EventRequestDTO event, Long id);

    void startProcessing(Long eventRequestId, String email);

    void approveProcessing(Long id);

    void rejectProcessing(Long id);

    Page<EventRequest> getAllEventsByStatus(EventRequestStatus status, Pageable pageable);
}
