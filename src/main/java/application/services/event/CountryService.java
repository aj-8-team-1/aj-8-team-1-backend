package application.services.event;

import application.models.event.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface CountryService {
    Map<String, List<String>> getGroupedCities();

    Page<Country> getAllCountries(Pageable pageable);

    Country getByIdCountry(Long id);

    void createCountry(String countryName);

    void deleteCountry(String countryName);

    void updateByIdCountry(Country country, Long id);
}
