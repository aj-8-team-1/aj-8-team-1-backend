package application.services.event;

import application.models.event.Website;

import java.util.List;

public interface WebsiteService {
    Website getByIdWebsite(Long id);

    void createWebsite(Website website);

    void updateByIdWebsite(Long id, Website updatedWebsite);

    void deleteByIdWebsite(Long id);

    List<Website> getWebsiteList(List<Long> websiteIds);

    Website getWebsiteByTitle(String title);

    List<Website> getNewWebsites(List<String> websiteTitles);

}
