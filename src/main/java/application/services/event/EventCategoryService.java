package application.services.event;

import application.models.event.EventCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EventCategoryService {
    Page<EventCategory> getAllEventCategory(Pageable pageable);

    List<EventCategory> getAll();

    List<EventCategory> getActive();

    EventCategory getByIdEventCategory(Long id);

    void createEventCategory(EventCategory eventCategory);

    void deleteByIdEventCategory(Long id);

    void updateByIdEventCategory(EventCategory eventCategory, Long id);

    EventCategory getEventCategoryByTitleIgnoreCase(String title);

}
