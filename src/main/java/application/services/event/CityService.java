package application.services.event;

import application.models.event.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CityService {
    List<String> getCitiesByCountry(String countryName);

    Page<City> getAllCities(Pageable pageable);

    List<City> getAll();

    City getByIdCity(Long id);

    void createCity(String countryName, String cityName);

    void deleteCity(String cityName);

    void updateByIdCity(City city, Long id);

    City getCityByTitle(String title);
}
