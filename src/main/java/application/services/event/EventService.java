package application.services.event;

import application.DTO.events.EventDTO;
import application.DTO.images.ImageDTO;
import application.eventsSearch.EventSearchCriteria;
import application.models.event.City;
import application.models.event.Event;
import application.models.event.EventCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EventService {
    Page<Event> getAllEvents(Pageable pageable);

    Long createEvent(EventDTO event);

    void createEventImages(ImageDTO image, Long eventId);

    Event getEventById(Long id);

    void deleteEventById(Long eventId);

    void updateEventById(EventDTO event, Long id);

    List<Event> searchEvents(EventSearchCriteria eventCriteria);

    void save(Event event);

    List<Event> getEventsByCategory(EventCategory eventCategory);

    void addRecommendation(Long id);

    void retractRecommendation(Long id);

    List<Event> getEventsByFormat(String format);

    List<Event> getEventsByCity(City city);

    List<Event> getEventsByFormatAndCity(String format, City city);
}