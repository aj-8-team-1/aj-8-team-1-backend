package application.services.event;

import application.models.Image;
import application.models.event.City;
import application.models.event.Place;
import application.placesSearch.PlaceSearchCriteria;

import java.util.List;

public interface PlaceService {

    Place getPlaceById(Long id);

    List<Place> getAll();

    List<Place> getActive();

    Place getPlaceByTitle(String searchPlace);

    void createPlace(Place place, Image image);

    void createPlaceWithImage(String title, String square, String capacity, String aspectRatio, String guests, String rate, String address, Image image, City city);

    void updatePlace(Long id, Place updatedPlace);

    void uploadImage(Long id, Image image);

    List<Place> findPlaceByCity(City city);

    List<Place> searchPlaces(PlaceSearchCriteria placeCriteria);
}
