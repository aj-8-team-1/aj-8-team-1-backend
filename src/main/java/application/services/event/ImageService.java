package application.services.event;

import application.models.Image;

public interface ImageService {

    String upload(Image image);
    void delete(String fileName);

}
