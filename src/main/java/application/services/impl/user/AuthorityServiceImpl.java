package application.services.impl.user;

import application.models.user.Authority;
import application.repositories.user.AuthorityRepository;
import application.services.user.AuthorityService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {
    private static final Logger logger = LoggerFactory.getLogger(AuthorityServiceImpl.class);
    private final AuthorityRepository repository;

    @Override
    public List<Authority> findAllByAuthority(String authority) {
        logger.info("Finding all authorities by authority: {}", authority);
        List<Authority> authorities = repository.findAllByAuthority(authority);
        if (authorities.isEmpty()) {
            logger.warn("No authorities found for authority: {}", authority);
        } else {
            logger.info("Found {} authorities for authority: {}", authorities.size(), authority);
        }
        return authorities;
    }

    @Override
    public Authority findByAuthority(String authority) {
        return repository.findByAuthority(authority);
    }
}