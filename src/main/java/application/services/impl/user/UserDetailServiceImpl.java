package application.services.impl.user;

import application.models.user.User;
import application.models.user.UserDetail;
import application.repositories.user.UserDetailRepository;
import application.services.user.UserDetailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class UserDetailServiceImpl implements UserDetailService {
    private final UserDetailRepository repository;

    @Override
    public UserDetail getUserDetailById(Long userDetailId) {
        log.info("Getting UserDetail by ID: {}", userDetailId);
        return repository.findById(userDetailId).orElse(null);
    }

    @Override
    public void saveUserDetail(UserDetail userDetail) {
        log.info("Saving UserDetail by ID: {}", userDetail.getId());
        repository.save(userDetail);
        log.info("UserDetail with ID {} saving successfully", userDetail.getId());
    }

    @Override
    public void deleteUserDetail(Long userDetailId) {
        log.info("Deleting UserDetail by ID: {}", userDetailId);
        repository.deleteById(userDetailId);
        log.info("UserDetail with ID {} deleted successfully", userDetailId);
    }

    @Override
    public void updateUserDetail(User user, UserDetail updatedUserDetail) {
        UserDetail userDetail = user.getUserDetail();

        userDetail.setAge(updatedUserDetail.getAge());
        userDetail.setGender(updatedUserDetail.getGender());
        userDetail.setPhoneNumber(updatedUserDetail.getPhoneNumber());
        userDetail.setPosition(updatedUserDetail.getPosition());
        userDetail.setCompany(updatedUserDetail.getCompany());
        userDetail.setCity(updatedUserDetail.getCity());

        saveUserDetail(userDetail);

        log.info("User details {} has been successfully changed", user.getEmail());
    }
}