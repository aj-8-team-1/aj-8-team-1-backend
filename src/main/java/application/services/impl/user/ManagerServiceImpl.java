package application.services.impl.user;

import application.DTO.managers.CreateManagerDTO;
import application.constants.ApplicationConstants;
import application.eventsMail.EmailEvent;
import application.models.user.Manager;
import application.models.user.Token;
import application.repositories.user.ManagerRepository;
import application.services.user.AuthorityService;
import application.services.user.ManagerService;
import application.services.user.TokenService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private static final Logger logger = LoggerFactory.getLogger(ManagerServiceImpl.class);
    private final ManagerRepository repository;
    private final AuthorityService service;
    private final PasswordEncoder encoder;
    private final TokenService tokenService;
    private final ApplicationEventPublisher publisher;

    @Override
    public Manager register(CreateManagerDTO managerDTO) {
        String email = managerDTO.getEmail();

        if (repository.existsManagerByEmail(email)) {
            throw new IllegalArgumentException("Пользователь с email - %s уже существует".formatted(email));
        }

        String password = managerDTO.getPassword();

        Manager newInstance = Manager.builder()
                .email(email)
                .password(encoder.encode(password))
                .firstName(managerDTO.getFirstName())
                .lastName(managerDTO.getLastName())
                .authorities(service.findAllByAuthority(managerDTO.getAuthority()))
                .build();

        repository.save(newInstance);
//
//        String templatePath = ApplicationConstants.REGISTRATION_EMAIL_TEMPLATE_PATH.getValue();
//        EmailEvent event = new EmailEvent(newInstance, EmailEvent.EventType.REGISTRATION_COMPLETE, templatePath);
//
//        publisher.publishEvent(event);

        logger.info("Registered new manager: {}", email);
        return newInstance;
    }

    @Override
    public Manager loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.info("Loading manager by username: {}", email);
        Optional<Manager> manager = repository.findByEmail(email);

        if (manager.isEmpty()) {
            throw new UsernameNotFoundException("Пользователь с именем %s не существует!".formatted(email));
        }

        return manager.get();
    }

    @Override
    public List<Manager> findALl() {
        logger.info("Fetching all managers");
        return repository.findAll();
    }

    @Override
    @Transactional
    public void enable(String token) {
        Long id = tokenService.get(token, "emails:").orElseThrow(() -> new EntityNotFoundException("Пользователь не найден")).getUserID();
        Manager manager = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Пользователь с идентификатором %s не найден".formatted(id)));
        manager.setEnabled(true);

        repository.save(manager);
        logger.info("Enabled manager with ID: {}", id);
    }

    @Override
    @Transactional
    public void resetPassword(String token, String newPassword) {
        Token resetToken = tokenService.get(token, "passwordReset:").orElseThrow(() -> new IllegalArgumentException("Неверный или истекший токен сброса"));

        Manager manager = repository.findById(resetToken.getUserID()).orElseThrow(() -> new EntityNotFoundException("Пользователь не найден"));

        manager.setPassword(encoder.encode(newPassword));
        repository.save(manager);

        tokenService.invalidateToken(token, "passwordReset:");
        logger.info("Reset password for manager with ID: {}", resetToken.getUserID());
    }

    @Override
    public void requestPasswordReset(String email) {
        logger.info("Requesting password reset for manager with email: {}", email);
        Manager manager = loadUserByUsername(email);

        String templatePath = ApplicationConstants.RESET_PASSWORD_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(manager, EmailEvent.EventType.RESET_PASSWORD, templatePath);

        publisher.publishEvent(event);
    }
}