package application.services.impl.user;

import application.models.user.Token;
import application.services.user.TokenService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor
public class TokenServiceImpl implements TokenService {
    private static final Logger logger = LoggerFactory.getLogger(TokenServiceImpl.class);
    private static final long EXPIRATION = 60 * 3;
    private final RedisTemplate<String, Token> redis;

    @Override
    public void set(Token token, String teg) {
        String key = teg + token.getToken();
        redis.opsForValue().set(key, token);
        redis.expire(key, EXPIRATION, TimeUnit.MINUTES);
        logger.info("Token set successfully: {}", key);
    }

    @Override
    public Optional<Token> get(String id, String teg) {
        String key = teg + id;
        Token token = redis.opsForValue().get(key);
        logger.info("Token retrieved successfully: {}", key);
        return Optional.ofNullable(token);
    }

    @Override
    public void invalidateToken(String token, String teg) {
        String key = teg + token;
        redis.delete(key);
        logger.info("Token invalidated successfully: {}", key);
    }
}
