package application.services.impl.user;

import application.DTO.users.CreateUserDTO;
import application.DTO.users.UpdateUserDTO;
import application.constants.ApplicationConstants;
import application.eventsMail.EmailEvent;
import application.models.Image;
import application.models.user.*;
import application.repositories.user.UserRepository;
import application.services.event.ImageService;
import application.services.user.AuthorityService;
import application.services.user.TokenService;
import application.services.user.UserDetailService;
import application.services.user.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository repository;
    private final AuthorityService service;
    private final PasswordEncoder encoder;
    private final TokenService tokenService;
    private final ApplicationEventPublisher publisher;
    private final UserDetailService userDetailService;
    private final ImageService imageService;

    @Override
    public void register(CreateUserDTO user) {
        String email = user.getEmail();

        if (repository.existsUserByEmail(email)) {
            throw new IllegalArgumentException("Пользователь с email - %s уже существует".formatted(email));
        }

        String password = user.getPassword();

        UserDetail userDetail = new UserDetail();

        User newInstance = User.builder()
                .email(email)
                .password(encoder.encode(password))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .authorities(service.findAllByAuthority(user.getAuthority()))
                .provider(Provider.LOCAL)
                .userDetail(userDetail)
                .build();

        repository.save(newInstance);
        userDetailService.saveUserDetail(userDetail);

        String templatePath = ApplicationConstants.REGISTRATION_EMAIL_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(newInstance, EmailEvent.EventType.REGISTRATION_COMPLETE, templatePath);

        publisher.publishEvent(event);

        logger.info("User registered successfully: {}", newInstance.getEmail());
    }

    @Override
    public User loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.info("Loading user by username: {}", email);
        Optional<User> user = repository.findByEmail(email);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("Пользователь с именем %s не существует!".formatted(email));
        }
        return user.get();
    }

    @Override
    public List<User> findALl() {
        logger.info("Fetching all users");
        return repository.findAll();
    }

    @Override
    @Transactional
    public void enable(String token) {
        Long id = tokenService.get(token, "emails:").orElseThrow(() -> new EntityNotFoundException("Пользователь не найден")).getUserID();
        User user = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Пользователь с идентификатором %s не найден".formatted(id)));
        user.setEnabled(true);

        repository.save(user);
        logger.info("User with ID {} enabled", id);
    }

    @Override
    @Transactional
    public void resetPassword(String token, String newPassword) {
        Token resetToken = tokenService.get(token, "passwordReset:").orElseThrow(() -> new IllegalArgumentException("Неверный или истекший токен сброса"));

        User user = repository.findById(resetToken.getUserID()).orElseThrow(() -> new EntityNotFoundException("Пользователь не найден"));

        user.setPassword(encoder.encode(newPassword));
        repository.save(user);

        tokenService.invalidateToken(token, "passwordReset:");
        logger.info("Password reset successfully for user with ID {}", resetToken.getUserID());
    }

    @Override
    public void requestPasswordReset(String email) {
        logger.info("Requesting password reset for user with email: {}", email);
        User user = loadUserByUsername(email);

        String templatePath = ApplicationConstants.RESET_PASSWORD_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(user, EmailEvent.EventType.RESET_PASSWORD, templatePath);

        publisher.publishEvent(event);
    }

    @Override
    public void processOAuthPostLogin(CustomOAuth2User oauthUser) {
        Map<String, Object> attributes = oauthUser.getAttributes();
        Optional<User> existUser = repository.findByEmail(oauthUser.getEmail());

        if (existUser.isEmpty()) {

            String firstName = (String) attributes.get("given_name");
            String lastName = (String) attributes.get("family_name");
            String uuid = UUID.randomUUID().toString();
            UserDetail userDetail = new UserDetail();

            User newUser = User.builder()
                    .email(oauthUser.getEmail())
                    .provider(Provider.GOOGLE)
                    .isEnabled(true)
                    .firstName(firstName)
                    .lastName(lastName)
                    .password(encoder.encode(uuid))
                    .authorities(service.findAllByAuthority("USER"))
                    .userDetail(userDetail)
                    .build();

            repository.save(newUser);
            userDetailService.saveUserDetail(userDetail);
        } else
            System.out.println("Пользователь с адресом электронной почты " + oauthUser.getEmail() + " уже существует.");
    }

    @Override
    public void save(User user) {
        repository.save(user);
        logger.info("user with email: {} successfully saved", user.getEmail());
    }

    @Override
    public void updateUser(String username, UpdateUserDTO updateUserDTO) {
        User user = loadUserByUsername(username);

        user.setFirstName(updateUserDTO.getFirstName());
        user.setLastName(updateUserDTO.getLastName());

        save(user);

        logger.info("User data {} has been successfully changed", user.getEmail());
    }

    @Override
    public void isCompanyConfirmed(User user) {
        UserDetail userDetail = user.getUserDetail();
        userDetail.setCompanyConfirmation(true);
        userDetailService.saveUserDetail(userDetail);
        logger.info("Company confirmation for ID {} was successful", userDetail.getId());

        addRoleToUser(user);
    }

    private void addRoleToUser(User user) {
        logger.info("Adding an exponent role for a user {}", user.getEmail());
        List<Authority> authorities = new ArrayList<>(user.getAuthorities());
        Authority authority = service.findByAuthority("EXPONENT");
        if (authority != null && authority.getAuthority() != null) {
            authorities.add(authority);

            user.setAuthorities(authorities);

            save(user);
            logger.info("The exponent role for user {} has been successfully added", user.getEmail());
        } else {
            logger.error("Cannot add null authority to user {}", user.getEmail());
        }
    }

    @Override
    public void uploadImage(User user, Image image) {
        UserDetail userDetail = user.getUserDetail();
        String currentImage = userDetail.getImage();

        if (currentImage != null) {
            imageService.delete(currentImage);
        }
        String fileName = imageService.upload(image);

        userDetail.setImage(fileName);

        userDetailService.saveUserDetail(userDetail);
    }
}