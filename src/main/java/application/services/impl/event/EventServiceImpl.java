package application.services.impl.event;

import application.DTO.events.EventDTO;
import application.DTO.images.ImageDTO;
import application.eventsSearch.EventSearchCriteria;
import application.eventsSearch.EventSpecifications;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.event.*;
import application.repositories.event.EventRepository;
import application.services.event.*;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class EventServiceImpl implements EventService {
    private static final Logger logger = LoggerFactory.getLogger(EventServiceImpl.class);
    private final EventRepository eventRepository;
    private final CityService cityService;
    private final OrganizerService organizerService;
    private final EventCategoryService eventCategoryService;
    private final ImageService imageService;
    private final TagService tagService;
    private final WebsiteService websiteService;
    private final PlaceService placeService;
    private final ImageMapper imageMapper;

    @Override
    public Page<Event> getAllEvents(Pageable pageable) {
        logger.info("Getting all events");
        return eventRepository.findAll(pageable);
    }

    @Override
    public Long createEvent(EventDTO event) {
        logger.info("Creating event: {}", event.getTitle());
        City city = cityService.getByIdCity(event.getCityIds());
        Organizer organizer = organizerService.getByIdOrganizer(event.getOrganizerIds());
        Website website = websiteService.getWebsiteByTitle(event.getWebsite());
        EventCategory eventCategory = eventCategoryService.getByIdEventCategory(event.getCategoryIds());
        List<Tag> tags = tagService.getTagList(event.getTagIds());
        Place place = placeService.getPlaceById(event.getPlaceId());

        Event newEvent = Event.builder()
                .title(event.getTitle())
                .startDate(event.getStartDate())
                .endDate(event.getEndDate())
                .description(event.getDescription())
                .format(event.getFormat())
                .city(city)
                .organizer(organizer)
                .websites(Collections.singletonList(website))
                .eventCategory(eventCategory)
                .tags(tags)
                .place(place)
                .build();

        eventRepository.save(newEvent);
        logger.info("Event {} created successfully", event.getTitle());
        return newEvent.getId();
    }

    @Override
    public void createEventImages(ImageDTO imageDTO, Long eventId) {
        Event event = getEventById(eventId);
        Image image = imageMapper.toEntity(imageDTO);
        String fileName = imageService.upload(image);
        event.getImages().add(fileName);

        eventRepository.save(event);
    }

    @Override
    public Event getEventById(Long id) {
        logger.info("Getting event by ID: {}", id);
        return eventRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Event с таким " + id + " не найден"));
    }

    @Override
    public void deleteEventById(Long eventId) {
        logger.info("Deleting event with ID: {}", eventId);
        eventRepository.deleteById(eventId);
        logger.info("Event with ID {} deleted successfully", eventId);
    }

    @Override
    public void updateEventById(EventDTO event, Long id) {
        logger.info("Updating event with ID: {}", id);
        Event existingEvent = eventRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Event с таким " + id + " не найден"));

        existingEvent.setTitle(event.getTitle());
        existingEvent.setStartDate(event.getStartDate());
        existingEvent.setDescription(event.getDescription());
        existingEvent.setEndDate(event.getEndDate());
        existingEvent.setFormat(event.getFormat());

        existingEvent.setCity(cityService.getByIdCity(event.getCityIds()));
        existingEvent.setOrganizer(organizerService.getByIdOrganizer(event.getOrganizerIds()));
        existingEvent.setWebsites(Collections.singletonList(websiteService.getWebsiteByTitle(event.getWebsite())));
        existingEvent.setEventCategory(eventCategoryService.getByIdEventCategory(event.getCategoryIds()));
        existingEvent.setTags(tagService.getTagList(event.getTagIds()));

        eventRepository.save(existingEvent);
        logger.info("Event with ID {} updated successfully", id);
    }

    @Override
    public List<Event> searchEvents(EventSearchCriteria eventCriteria) {
        logger.info("Searching events with criteria: {}", eventCriteria);
        Specification<Event> specification = EventSpecifications.buildSpecification(eventCriteria);
        return eventRepository.findAll(specification);
    }


    @Override
    public void save(Event event) {
        eventRepository.save(event);
    }

    @Override
    public List<Event> getEventsByCategory(EventCategory eventCategory) {
        return eventRepository.findEventByEventCategory(eventCategory);
    }

    @Override
    public void addRecommendation(Long id) {
        Event event = getEventById(id);
        event.setRecommendation(true);
        save(event);
    }

    @Override
    public void retractRecommendation(Long id) {
        Event event = getEventById(id);
        event.setRecommendation(false);
        save(event);
    }
    @Override
    public List<Event> getEventsByFormat(String format){
        return eventRepository.findByFormat(format);
    }
    @Override
    public List<Event> getEventsByCity(City city){
        return eventRepository.findByCity(city);
    }
    @Override
    public List<Event> getEventsByFormatAndCity(String format, City city) {
        return eventRepository.findByFormatAndCity(format, city);
    }
}
