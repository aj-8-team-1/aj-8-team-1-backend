package application.services.impl.event;

import application.DTO.events.EventRequestDTO;
import application.DTO.images.ImageDTO;
import application.constants.ApplicationConstants;
import application.eventsMail.EmailEvent;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.event.*;
import application.models.user.User;
import application.models.user.UserDetail;
import application.repositories.EventRequestRepository;
import application.services.event.*;
import application.services.user.ManagerService;
import application.services.user.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class EventRequestServiceImpl implements EventRequestService {
    private final EventRequestRepository repository;
    private final EventService eventService;
    private final CityService cityService;
    private final OrganizerService organizerService;
    private final EventCategoryService eventCategoryService;
    private final ImageService imageService;
    private final TagService tagService;
    private final WebsiteService websiteService;
    private final ManagerService managerService;
    private final PlaceService placeService;
    private final UserService userService;
    private final ApplicationEventPublisher publisher;
    private final ImageMapper imageMapper;

    @Override
    public Page<EventRequest> getAllEvents(Pageable pageable) {
        log.info("Receive all event requests");
        return repository.findAll(pageable);
    }

    @Override
    public EventRequest getEventById(Long id) {
        log.info("Getting event request by ID: {}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Event request с таким " + id + " не найден"));
    }

    @Transactional
    @Override
    public Long createEvent(EventRequestDTO event, String username) {
        User user = userService.loadUserByUsername(username);

        log.info("Creating request event: {}", event.getTitle());
        City city = cityService.getByIdCity(event.getCityIds());
        List<Website> websites = websiteService.getNewWebsites(event.getWebsites());
        EventCategory eventCategory = eventCategoryService.getByIdEventCategory(event.getCategoryIds());
        List<Tag> tags = tagService.getTagList(event.getTagIds());
        Place place = placeService.getPlaceById(event.getPlaceId());

        EventRequest newEvent = EventRequest.builder()
                .title(event.getTitle())
                .description(event.getDescription())
                .format(event.getFormat())
                .startDate(event.getStartDate())
                .endDate(event.getEndDate())
                .status(EventRequestStatus.NEW)
                .city(city)
                .user(user)
                .websites(websites)
                .eventCategory(eventCategory)
                .tags(tags)
                .place(place)
                .comment(event.getComment())
                .build();

        repository.save(newEvent);
        log.info("Event {} created successfully", event.getTitle());
        return newEvent.getId();
    }

    @Override
    public void createEventRequestImages(ImageDTO imageDTO, Long eventId) {
        EventRequest event = getEventById(eventId);
        Image image = imageMapper.toEntity(imageDTO);
        String fileName = imageService.upload(image);
        event.getImages().add(fileName);

        repository.save(event);
    }

    @Override
    public void startProcessing(Long eventRequestId, String email) {
        EventRequest eventRequest = getEventById(eventRequestId);
        log.info("Process request event: {}", eventRequest.getTitle());

        eventRequest.setStatus(EventRequestStatus.PROCESS);
        eventRequest.setManager(managerService.loadUserByUsername(email));

        repository.save(eventRequest);
        log.info("Event request {} processed successfully", eventRequest.getTitle());

        String templatePath = ApplicationConstants.EVENT_REQUEST_IN_PROCESS_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(eventRequest.getUser(), EmailEvent.EventType.REQUEST_PROCESSING, templatePath);

        publisher.publishEvent(event);
    }

    @Override
    public void approveProcessing(Long id) {
        EventRequest eventRequest = getEventById(id);
        log.info("Finish request event: {}", eventRequest.getTitle());

        eventRequest.setStatus(EventRequestStatus.FINISH);

        repository.save(eventRequest);

        UserDetail userDetail = eventRequest.getUser().getUserDetail();

        Organizer existingOrganizer = organizerService.getByTitle(userDetail.getCompany());
        Organizer organizer;

        if (existingOrganizer != null) {
            organizer = existingOrganizer;
        } else {
            organizer = Organizer
                    .builder()
                    .phoneNumber(userDetail.getPhoneNumber())
                    .title(userDetail.getCompany())
                    .build();
            organizerService.saveOrganizer(organizer);
        }

        Event approveEvent = Event.builder()
                .title(eventRequest.getTitle())
                .startDate(eventRequest.getStartDate())
                .description(eventRequest.getDescription())
                .endDate(eventRequest.getEndDate())
                .format(eventRequest.getFormat())
                .city(eventRequest.getCity())
                .organizer(organizer)
                .websites(eventRequest.getWebsites())
                .eventCategory(eventRequest.getEventCategory())
                .tags(eventRequest.getTags())
                .images(eventRequest.getImages())
                .place(eventRequest.getPlace())
                .build();

        eventService.save(approveEvent);
        deleteEventRequest(eventRequest.getId());
        log.info("Event {} approved successfully", approveEvent.getTitle());

        String templatePath = ApplicationConstants.EVENT_REQUEST_IS_APPROVED_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(eventRequest.getUser(), EmailEvent.EventType.REQUEST_APPROVE, templatePath, approveEvent.getId());

        publisher.publishEvent(event);
    }

    @Override
    public void rejectProcessing(Long id) {
        EventRequest eventRequest = getEventById(id);
        log.info("Rejecting request event: {}", eventRequest.getTitle());

        String templatePath = ApplicationConstants.EVENT_REQUEST_IS_CANCELED_TEMPLATE_PATH.getValue();
        EmailEvent event = new EmailEvent(eventRequest.getUser(), EmailEvent.EventType.REQUEST_REJECT, templatePath, eventRequest);

        publisher.publishEvent(event);

        if (EventRequestStatus.PROCESS.equals(eventRequest.getStatus())) {
            repository.delete(eventRequest);
            log.info("Event request {} deleted successfully after rejection", eventRequest.getTitle());
        } else {
            log.warn("Cannot delete event request {} as it is not in 'finish' status", eventRequest.getTitle());
        }
    }

    @Override
    public Page<EventRequest> getAllEventsByStatus(EventRequestStatus status, Pageable pageable) {
        return repository.findByStatus(status, pageable);
    }

    @Override
    public void deleteEventRequest(Long id) {
        EventRequest eventRequest = getEventById(id);
        if (eventRequest.getStatus().equals(EventRequestStatus.FINISH)) {
            deleteEventById(eventRequest.getId());
        } else {
            log.info("event request with id {} has no deletion permission", id);
        }
    }

    @Override
    public void deleteEventById(Long id) {
        log.info("Deleting event request with ID: {}", id);
        repository.deleteById(id);
        log.info("Event request with ID {} deleted successfully", id);
    }

    @Override
    public void updateEventById(EventRequestDTO event, Long id) {
        log.info("Updating event request with ID: {}", id);
        EventRequest existingEvent = getEventById(id);

        existingEvent.setTitle(event.getTitle());
        existingEvent.setDescription(event.getDescription());
        existingEvent.setFormat(event.getFormat());
        existingEvent.setComment(event.getComment());

        existingEvent.setStartDate(event.getStartDate());
        existingEvent.setEndDate(event.getEndDate());

        existingEvent.setCity(cityService.getByIdCity(event.getCityIds()));
        existingEvent.setWebsites(websiteService.getNewWebsites(event.getWebsites()));
        existingEvent.setEventCategory(eventCategoryService.getEventCategoryByTitleIgnoreCase(event.getTitle()));
        existingEvent.setTags(tagService.getTagList(event.getTagIds()));

        repository.save(existingEvent);
        log.info("Event request with ID {} updated successfully", id);
    }
}
