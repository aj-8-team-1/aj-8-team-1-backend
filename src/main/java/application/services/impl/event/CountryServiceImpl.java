package application.services.impl.event;

import application.exceptions.EntityExistsException;
import application.models.event.City;
import application.models.event.Country;
import application.repositories.event.CityRepository;
import application.repositories.event.CountryRepository;
import application.services.event.CountryService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CountryServiceImpl implements CountryService {
    private static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;

    @Override
    public Map<String, List<String>> getGroupedCities() {
        logger.info("Getting grouped cities");
        List<Country> countries = countryRepository.findAll();
        Map<String, List<String>> groupedCities = new HashMap<>();
        for (Country country : countries) {
            List<City> cities = cityRepository.findByCountry(country);
            List<String> cityNames = cities.stream()
                    .map(City::getTitle)
                    .collect(Collectors.toList());
            groupedCities.put(country.getTitle(), cityNames);
        }
        return groupedCities;
    }

    @Override
    public Page<Country> getAllCountries(Pageable pageable) {
        logger.info("Getting all countries");
        return countryRepository.findAll(pageable);
    }

    @Override
    public Country getByIdCountry(Long id) {
        logger.info("Getting country by ID: {}", id);
        return countryRepository.findById(id).orElseThrow(() -> {
            logger.warn("Country with ID {} not found", id);
            return new EntityNotFoundException("Country с таким " + id + " не найден");
        });
    }

    @Override
    public void createCountry(String countryName) {
        logger.info("Creating country with name {}", countryName);
        Optional<Country> existingCountry = countryRepository.findByTitle(countryName);
        if (existingCountry.isPresent()) {
            logger.warn("Country with name {} already exists", countryName);
            throw new EntityExistsException("Country с именем " + countryName + " уже существует");
        } else {
            Country country = new Country();
            country.setTitle(countryName);
            countryRepository.save(country);
            logger.info("Country {} created successfully", countryName);
        }
    }

    @Override
    public void deleteCountry(String countryName) {
        logger.info("Deleting country with name {}", countryName);
        Optional<Country> countryOptional = countryRepository.findByTitle(countryName);
        countryOptional.ifPresent(country -> {
            countryRepository.delete(country);
            logger.info("Country {} deleted successfully", countryName);
        });
    }

    @Override
    public void updateByIdCountry(Country country, Long id) {
        logger.info("Updating country with ID {}", id);
        Country existingCountry = countryRepository.findById(id).orElseThrow(() -> {
            logger.warn("Country with ID {} not found", id);
            return new EntityNotFoundException("Country с таким " + id + " не найден");
        });
        existingCountry.setTitle(country.getTitle());
        countryRepository.save(existingCountry);
        logger.info("Country with ID {} updated successfully", id);
    }
}
