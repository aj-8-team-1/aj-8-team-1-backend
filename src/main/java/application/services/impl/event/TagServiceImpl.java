package application.services.impl.event;

import application.models.event.Tag;
import application.repositories.event.TagRepository;
import application.services.event.TagService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class TagServiceImpl implements TagService {
    private static final Logger logger = LoggerFactory.getLogger(TagServiceImpl.class);
    private final TagRepository tagRepository;

    @Override
    public Tag getByIdTag(Long id) {
        logger.info("Getting tag by ID: {}", id);
        return tagRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Tag с таким " + id + " не найден"));
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public List<Tag> getActive() {
        return tagRepository.findByActiveTrue();
    }

    @Override
    public void createTag(Tag tag) {
        tag.setActive(true);
        logger.info("Creating tag: {}", tag.getTitle());
        tagRepository.save(tag);
        logger.info("Tag {} created successfully", tag.getTitle());
    }

    @Override
    public void updateByIdTag(Long id, Tag updatedTag) {
        logger.info("Updating tag with ID: {}", id);
        Tag existingTag = tagRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Tag с таким " + id + " не найден"));
        existingTag.setTitle(updatedTag.getTitle());
        existingTag.setActive(updatedTag.isActive());
        tagRepository.save(existingTag);
        logger.info("Tag with ID {} updated successfully", id);
    }

    @Override
    public void deleteByIdTag(Long id) {
        logger.info("Deleting tag with ID: {}", id);
        tagRepository.deleteById(id);
        logger.info("Tag with ID {} deleted successfully", id);
    }

    @Override
    public List<Tag> getTagList(List<Long> tagIds) {
        List<Tag> tags = new ArrayList<>();
        for (Long tagId : tagIds) {
            Tag tag = getByIdTag(tagId);
            tags.add(tag);
        }
        return tags;
    }

    @Override
    public List<String> getNewTags(String tags) {
        return Arrays.asList(tags.split(" "));
    }

    @Override
    public Tag getTagByTitle(String title) {
        return tagRepository.getTagByTitleIgnoreCase(title);
    }
}
