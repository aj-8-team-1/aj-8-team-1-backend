package application.services.impl.event;

import application.models.event.Website;
import application.repositories.event.WebsiteRepository;
import application.services.event.WebsiteService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Flow;

@Service
@AllArgsConstructor
public class WebsiteServiceImpl implements WebsiteService {
    private static final Logger logger = LoggerFactory.getLogger(WebsiteServiceImpl.class);
    private final WebsiteRepository websiteRepository;

    @Override
    public Website getByIdWebsite(Long id) {
        logger.info("Getting website by ID: {}", id);
        return websiteRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Website с таким id " + id + " не найден"));
    }

    @Override
    public void createWebsite(Website website) {
        logger.info("Creating website: {}", website.getTitle());
        websiteRepository.save(website);
        logger.info("Website {} created successfully", website.getTitle());
    }

    @Override
    public void updateByIdWebsite(Long id, Website updatedWebsite) {
        logger.info("Updating website with ID: {}", id);
        Website existingWebsite = websiteRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Website с таким id " + id + " не найден"));
        existingWebsite.setTitle(updatedWebsite.getTitle());
        websiteRepository.save(existingWebsite);
        logger.info("Website with ID {} updated successfully", id);
    }

    @Override
    public void deleteByIdWebsite(Long id) {
        logger.info("Deleting website with ID: {}", id);
        websiteRepository.deleteById(id);
        logger.info("Website with ID {} deleted successfully", id);
    }

    @Override
    public List<Website> getWebsiteList(List<Long> websiteIds) {
        List<Website> websites = new ArrayList<>();
        for (Long websiteId : websiteIds) {
            Website website = getByIdWebsite(websiteId);
            websites.add(website);
        }
        return websites;
    }

    @Override
    public Website getWebsiteByTitle(String title) {
        return websiteRepository.getWebsiteByTitle(title);
    }

    @Override
    public List<Website> getNewWebsites(List<String> websiteTitles) {
        List<Website> newSites = new ArrayList<>();

        for (String title : websiteTitles) {
            Website website = getWebsiteByTitle(title);
            if (website == null) {
                Website newSite = Website.builder()
                        .title(title)
                        .build();
                websiteRepository.save(newSite);
                newSites.add(newSite);
            } else {
                newSites.add(website);
            }
        }
        return newSites;
    }

}
