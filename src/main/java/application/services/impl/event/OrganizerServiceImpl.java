package application.services.impl.event;

import application.models.event.Organizer;
import application.repositories.event.OrganizerRepository;
import application.services.event.OrganizerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrganizerServiceImpl implements OrganizerService {
    private static final Logger logger = LoggerFactory.getLogger(OrganizerServiceImpl.class);
    private final OrganizerRepository organizerRepository;

    @Override
    public Page<Organizer> getAllOrganizers(Pageable pageable) {
        logger.info("Getting all organizers");
        return organizerRepository.findAll(pageable);
    }
    @Override
    public List<Organizer> getAll(){
        return organizerRepository.findAll();
    }
    @Override
    public Organizer getByIdOrganizer(Long id) {
        logger.info("Getting organizer by ID: {}", id);
        return organizerRepository.getOrganizerById(id);
    }

    @Override
    public void createOrganizer(Organizer organizer) {
        logger.info("Creating organizer: {}", organizer.getTitle());
        organizerRepository.save(organizer);
        logger.info("Organizer {} created successfully", organizer.getTitle());
    }

    @Override
    public void deleteByIdOrganizer(Long id) {
        logger.info("Deleting organizer with ID: {}", id);
        organizerRepository.deleteById(id);
        logger.info("Organizer with ID {} deleted successfully", id);
    }

    @Override
    public void updateByIdOrganizer(Organizer organizer, Long id) {
        logger.info("Updating organizer with ID: {}", id);
        Organizer existingOrganizer = organizerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Organizer с таким " + id + " не найден"));
        existingOrganizer.setTitle(organizer.getTitle());
        existingOrganizer.setPhoneNumber(organizer.getPhoneNumber());
        organizerRepository.save(existingOrganizer);
        logger.info("Organizer with ID {} updated successfully", id);
    }

    @Override
    public void saveOrganizer(Organizer organizer) {
        organizerRepository.save(organizer);
    }

    @Override
    public Organizer getByTitle(String title) {
        return organizerRepository.findByTitleIgnoreCase(title);
    }
}
