package application.services.impl.event;

import application.DTO.images.ImageDTO;
import application.models.Image;
import application.mappers.ImageMapper;
import application.models.event.City;
import application.models.event.Place;
import application.placesSearch.PlaceSearchCriteria;
import application.placesSearch.PlaceSpecifications;
import application.repositories.event.PlaceRepository;
import application.services.event.CityService;
import application.services.event.ImageService;
import application.services.event.PlaceService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class PlaceServiceImpl implements PlaceService {
    private final PlaceRepository placeRepository;
    private final ImageService imageService;
    private final ImageMapper imageMapper;
    private final CityService cityService;
    private static final Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);

    @Override
    public Place getPlaceById(Long id) {
        logger.info("Getting city by ID: {}", id);
        return placeRepository.findById(id).orElseThrow(() -> {
            logger.warn("Place with ID {} not found", id);
            return new EntityNotFoundException("Place с таким " + id + " не найден");
        });
    }

    @Override
    public List<Place> getAll() {
        logger.info("Getting all places");
        return placeRepository.findAll();
    }

    @Override
    public List<Place> getActive() {
        logger.info("Getting active places");
        return placeRepository.findByActiveTrue();
    }

    @Override
    public Place getPlaceByTitle(String searchPlace) {
        logger.info("Getting place by name: {}", searchPlace);
        Place newInstance = Place.builder()
                .title(searchPlace)
                .build();
        placeRepository.save(newInstance);
        return newInstance;
    }

    @Override
    public void createPlace(Place place, Image image) {
        place.setActive(true);

        if (image != null && image.getFile() != null) {
            String fileName = imageService.upload(image);
            place.getImages().add(fileName);
        }
        logger.info("Creating tag: {}", place.getTitle());
        placeRepository.save(place);
        logger.info("Tag {} created successfully", place.getTitle());
    }

    @Override
    public void createPlaceWithImage(String title, String square, String capacity, String aspectRatio, String guests, String rate, String address, Image image, City city) {
        String fileName = imageService.upload(image);


        Place place = Place.builder()
                .title(title)
                .address(address)
                .square(square)
                .capacity(capacity)
                .aspectRatio(aspectRatio)
                .guests(guests)
                .rate(rate)
                .active(true)
                .images(Collections.singletonList(fileName))
                .city(city)
                .build();

        logger.info("Creating place: {}", place.getTitle());
        placeRepository.save(place);
        logger.info("Place {} created successfully", place.getTitle());
    }

    private void processImageForPlace(Place place, ImageDTO imageDto) {
        if (imageDto.getFile() != null) {
            Image image = imageMapper.toEntity(imageDto);
            String fileName = imageService.upload(image);
            place.getImages().add(fileName);
        }
    }

    @Override
    public void updatePlace(Long id, Place updatedPlace) {
        logger.info("Updating place with ID: {}", id);

        Place existingPlace = getPlaceById(id);
        existingPlace.setTitle(updatedPlace.getTitle());
        existingPlace.setAddress(updatedPlace.getAddress());
        existingPlace.setActive(updatedPlace.isActive());
        placeRepository.save(existingPlace);

        logger.info("Place with ID {} updated successfully", id);
    }

    @Override
    public void uploadImage(Long id, Image image) {
        logger.info("Uploading image for place with ID: {}", id);
        Place place = getPlaceById(id);
        String fileName = imageService.upload(image);
        place.getImages().add(fileName);
        placeRepository.save(place);
        logger.info("Image uploaded successfully for place with ID: {}", id);
    }
    @Override
    public List<Place> findPlaceByCity(City city) {
        logger.info("Finding places by city: {}", city);
        return placeRepository.findByCity(city);
    }

    public List<Place> searchPlaces(PlaceSearchCriteria placeCriteria) {
        logger.info("Searching places with criteria: {}", placeCriteria);
        Specification<Place> specification = PlaceSpecifications.buildSpecification(placeCriteria);
        return placeRepository.findAll(specification);
    }
}
