package application.services.impl.event;

import application.exceptions.CountryNotFoundException;
import application.exceptions.EntityExistsException;
import application.models.event.City;
import application.models.event.Country;
import application.repositories.event.CityRepository;
import application.repositories.event.CountryRepository;
import application.services.event.CityService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CityServiceImpl implements CityService {
    private static final Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);
    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;

    @Override
    public List<String> getCitiesByCountry(String countryName) {
        logger.info("Getting cities by country: {}", countryName);
        Optional<Country> countryOptional = countryRepository.findByTitle(countryName);
        if (countryOptional.isPresent()) {
            Country country = countryOptional.get();
            List<City> cities = cityRepository.findByCountry(country);
            return cities.stream()
                    .map(City::getTitle)
                    .collect(Collectors.toList());
        } else {
            logger.warn("Country with name {} not found", countryName);
            throw new CountryNotFoundException("Country с именем " + countryName + " не найдена");
        }
    }

    @Override
    public Page<City> getAllCities(Pageable pageable) {
        logger.info("Getting all cities");
        return cityRepository.findAll(pageable);
    }

    @Override
    public List<City> getAll() {
        return cityRepository.findAll();
    }

    @Override
    public City getByIdCity(Long id) {
        logger.info("Getting city by ID: {}", id);
        return cityRepository.findById(id).orElseThrow(() -> {
            logger.warn("City with ID {} not found", id);
            return new EntityNotFoundException("City с таким " + id + " не найден");
        });
    }

    @Override
    public void createCity(String countryName, String cityName) {
        logger.info("Creating city with name {} in country {}", cityName, countryName);
        Optional<City> existingCity = cityRepository.findByTitle(cityName);
        if (existingCity.isPresent()) {
            logger.warn("City with name {} already exists", cityName);
            throw new EntityExistsException("City с именем " + cityName + " уже существует");
        } else {
            Optional<Country> countryOptional = countryRepository.findByTitle(countryName);
            if (countryOptional.isPresent()) {
                Country country = countryOptional.get();
                City city = new City();
                city.setTitle(cityName);
                city.setCountry(country);
                cityRepository.save(city);
                logger.info("City {} created successfully in country {}", cityName, countryName);
            } else {
                logger.warn("Country with name {} not found", countryName);
                throw new CountryNotFoundException("Country с именем " + countryName + " не найдена");
            }
        }
    }

    @Override
    public void deleteCity(String cityName) {
        logger.info("Deleting city with name {}", cityName);
        Optional<City> cityOptional = cityRepository.findByTitle(cityName);
        cityOptional.ifPresent(city -> {
            cityRepository.delete(city);
            logger.info("City {} deleted successfully", cityName);
        });
    }


    @Override
    public void updateByIdCity(City city, Long id) {
        logger.info("Updating city with ID {}", id);
        City existingCity = cityRepository.findById(id).orElseThrow(() -> {
            logger.warn("City with ID {} not found", id);
            return new EntityNotFoundException("City с таким " + id + " не найден");
        });
        existingCity.setTitle(city.getTitle());
        cityRepository.save(existingCity);
        logger.info("City with ID {} updated successfully", id);
    }

    @Override
    public City getCityByTitle(String title) {
        Optional<City> city = cityRepository.findByTitle(title);
        Country country = Country.builder()
                .title("Новая Гвинея")
                .build();
        countryRepository.save(country);
        if (city.isEmpty()) {
            City newInstance = City.builder()
                    .title(title)
                    .country(country)
                    .build();
            cityRepository.save(newInstance);
            return newInstance;
        }
        return cityRepository.findByTitle(title).get();
    }
}
