package application.services.impl.event;

import application.models.event.EventCategory;
import application.repositories.event.EventCategoryRepository;
import application.services.event.EventCategoryService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EventCategoryServiceImpl implements EventCategoryService {
    private static final Logger logger = LoggerFactory.getLogger(EventCategoryServiceImpl.class);
    private final EventCategoryRepository eventCategoryRepository;

    @Override
    public Page<EventCategory> getAllEventCategory(Pageable pageable) {
        logger.info("Getting all event categories");
        return eventCategoryRepository.findAll(pageable);
    }

    @Override
    public List<EventCategory> getAll() {
        return eventCategoryRepository.findAll();
    }

    @Override
    public List<EventCategory> getActive() {
        return eventCategoryRepository.findByActiveTrue();
    }

    @Override
    public EventCategory getByIdEventCategory(Long id) {
        logger.info("Getting event category by ID: {}", id);
        return eventCategoryRepository.findById(id).orElseThrow(() -> {
            logger.warn("Event category with ID {} not found", id);
            return new EntityNotFoundException("EventCategory с таким " + id + " не найден");
        });
    }

    @Override
    public void createEventCategory(EventCategory eventCategory) {
        eventCategory.setActive(true);
        logger.info("Creating event category: {}", eventCategory.getTitle());
        eventCategoryRepository.save(eventCategory);
        logger.info("Event category {} created successfully", eventCategory.getTitle());
    }

    @Override
    public void deleteByIdEventCategory(Long id) {
        logger.info("Deleting event category with ID: {}", id);
        eventCategoryRepository.deleteById(id);
        logger.info("Event category with ID {} deleted successfully", id);
    }

    @Override
    public void updateByIdEventCategory(EventCategory eventCategory, Long id) {
        logger.info("Updating event category with ID: {}", id);
        EventCategory existingEventCategory = eventCategoryRepository.findById(id).orElseThrow(() -> {
            logger.warn("Event category with ID {} not found", id);
            return new EntityNotFoundException("EventCategory с таким " + id + " не найден");
        });
        existingEventCategory.setTitle(eventCategory.getTitle());
        existingEventCategory.setActive(eventCategory.isActive());
        eventCategoryRepository.save(existingEventCategory);
        logger.info("Event category with ID {} updated successfully", id);
    }

    @Override
    public EventCategory getEventCategoryByTitleIgnoreCase(String title) {
        logger.info("Getting event category by title (ignore case): {}", title);
        EventCategory category = eventCategoryRepository.getEventCategoryByTitleIgnoreCase(title);
        if (category == null) {
            EventCategory newInstance = EventCategory.builder()
                    .title(title)
                    .active(true)
                    .build();
            eventCategoryRepository.save(newInstance);
            logger.info("Event category {} created successfully (getEventCategoryByTitleIgnoreCase)", title);
            return newInstance;
        }
        logger.info("Event category {} retrieved successfully (getEventCategoryByTitleIgnoreCase)", title);
        return category;
    }
}
