package application.services.user;

import application.DTO.users.CreateUserDTO;
import application.DTO.users.UpdateUserDTO;
import application.models.Image;
import application.models.user.CustomOAuth2User;
import application.models.user.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void register(CreateUserDTO userDTO);

    User loadUserByUsername(String username);

    List<User> findALl();

    void enable(String token);

    void resetPassword(String token, String newPassword);

    void requestPasswordReset(String email);

    void processOAuthPostLogin(CustomOAuth2User oauthUser);

    void save(User user);

    void updateUser(String username, UpdateUserDTO updateUserDTO);

    void isCompanyConfirmed(User user);

    void uploadImage(User user, Image image);
}