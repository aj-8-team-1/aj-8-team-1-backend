package application.services.user;

import application.models.user.Authority;

import java.util.List;

public interface AuthorityService {

    List<Authority> findAllByAuthority(String authority);

    Authority findByAuthority(String authority);
}