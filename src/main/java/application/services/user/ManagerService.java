package application.services.user;

import application.DTO.managers.CreateManagerDTO;
import application.models.user.Manager;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface ManagerService extends UserDetailsService {

    Manager register(CreateManagerDTO managerDTO);

    Manager loadUserByUsername(String username);

    List<Manager> findALl();

    void enable(String token);

    void resetPassword(String token, String newPassword);

    void requestPasswordReset(String email);

}