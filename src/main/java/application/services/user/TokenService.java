package application.services.user;

import application.models.user.Token;

import java.util.Optional;

public interface TokenService {
    void set(Token token, String teg);

    Optional<Token> get(String id, String teg);

    void invalidateToken(String token, String teg);
}
