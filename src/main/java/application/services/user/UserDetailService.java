package application.services.user;

import application.models.user.User;
import application.models.user.UserDetail;

public interface UserDetailService {

    UserDetail getUserDetailById(Long userDetailId);

    void saveUserDetail(UserDetail userDetail);

    void deleteUserDetail(Long userDetailId);

    void updateUserDetail(User user, UserDetail updatedUserDetail);
}