package application.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

public interface AuthenticationFilter extends Filter {
    Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response);

    void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication);

    void setAuthenticationManager(AuthenticationManager authenticationManager);
}