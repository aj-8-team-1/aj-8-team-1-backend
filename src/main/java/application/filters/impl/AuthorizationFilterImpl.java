package application.filters.impl;

import application.exceptions.ApiError;
import application.filters.AuthorizationFilter;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
public class AuthorizationFilterImpl extends OncePerRequestFilter implements AuthorizationFilter {

    private final String jwtSecret;

    public AuthorizationFilterImpl(@Value("${jwt.secret}") String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    private static final AntPathMatcher pathMatcher = new AntPathMatcher();

    private static final Set<String> EXCLUDED_PATHS = Set.of(
            "/users/login",
            "/users/registration",
            "/tokens/refresh",
            "/users/confirm",
            "/users/password/recovery",
            "/users/password/reset",
            "/oauth2/authorization/google",
            "/managers/registration",
            "/managers/login",
            "/events",
            "/events/search",
            "/events/city/**",
            "/events/byFormat/**",
            "/events/filter",
            "/events/category/**",
            "/categories",
            "/places",
            "/places/search",
            "/places/active",
            "/categories/active"
    );

    @Override
    public void doFilterInternal(@NonNull HttpServletRequest request,
                                 @NonNull HttpServletResponse response,
                                 @NonNull FilterChain filterChain) throws ServletException, IOException {
        boolean isExcluded = EXCLUDED_PATHS.stream()
                .anyMatch(p -> pathMatcher.match(p, request.getServletPath()));

        if (isExcluded) {
            filterChain.doFilter(request, response);
        } else {
            try {
                String header = request.getHeader(HttpHeaders.AUTHORIZATION);

                if (header != null && header.startsWith("Bearer ")) {
                    String bearer = header.substring("Bearer ".length());
                    Algorithm algorithm = Algorithm.HMAC256(jwtSecret.getBytes());

                    JWTVerifier verifier = JWT
                            .require(algorithm)
                            .build();

                    DecodedJWT decodedJWT = verifier.verify(bearer);

                    String username = decodedJWT.getSubject();
                    String[] roles = decodedJWT
                            .getClaim("authorities")
                            .asArray(String.class);

                    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    Arrays.asList(roles).forEach(authority -> authorities.add(new SimpleGrantedAuthority(authority)));

                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(token);

                    filterChain.doFilter(request, response);
                }
            } catch (Exception exception) {
                ApiError apiError = new ApiError(LocalDateTime.now(), exception.getMessage(), HttpStatus.FORBIDDEN.value());

                response.setStatus(HttpStatus.FORBIDDEN.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), apiError);
            }
        }
    }
}

