package application.filters.impl;

import application.authenticationManager.impl.ManagerAuthentication;
import application.filters.AuthenticationFilter;
import application.models.user.Authority;
import application.models.user.Manager;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ManagerAuthenticationFilterImpl extends UsernamePasswordAuthenticationFilter implements AuthenticationFilter {
    private final ManagerAuthentication managerAuthentication;
    private final String jwtSecret;
    private final long accessTokenExpirationTime;
    private final long refreshTokenExpirationTime;

    public ManagerAuthenticationFilterImpl(ManagerAuthentication managerAuthentication,
                                           @Value("${jwt.secret}") String jwtSecret,
                                           @Value("600000") long accessTokenExpirationTime,
                                           @Value("1800000") long refreshTokenExpirationTime) {
        this.managerAuthentication = managerAuthentication;
        this.jwtSecret = jwtSecret;
        this.accessTokenExpirationTime = accessTokenExpirationTime;
        this.refreshTokenExpirationTime = refreshTokenExpirationTime;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            Manager manager = new ObjectMapper().readValue(request.getInputStream(), Manager.class);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(manager.getUsername(), manager.getPassword());
            return managerAuthentication.authenticate(token);
        } catch (IOException e) {
            throw new RuntimeException("Ошибка при чтении учетных данных", e);
        }
    }

    @Override
    public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) {
        Manager manager = (Manager) authentication.getPrincipal();
        List<String> authorities = manager
                .getAuthorities()
                .stream()
                .map(Authority::getAuthority)
                .toList();

        Algorithm algorithm = Algorithm.HMAC256(jwtSecret.getBytes());

        String accessToken = JWT
                .create()
                .withSubject(manager.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + accessTokenExpirationTime))
                .withIssuer(request.getRequestURI())
                .withClaim("authorities", authorities)
                .sign(algorithm);

        String refreshToken = JWT
                .create()
                .withSubject(manager.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + refreshTokenExpirationTime))
                .withIssuer(request.getRequestURI())
                .sign(algorithm);

        response.setHeader("access_token", accessToken);
        response.setHeader("refresh_token", refreshToken);
    }

    @Autowired
    public void setAuthenticationManager(ManagerAuthentication managerAuthentication) {
        super.setAuthenticationManager(managerAuthentication);
    }
}
