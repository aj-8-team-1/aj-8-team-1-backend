package application.mappers;

import application.DTO.images.ImageDTO;
import application.models.Image;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ImageMapper extends Mappable<Image, ImageDTO> {
}
