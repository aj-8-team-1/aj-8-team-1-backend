package application.authenticationManager.impl;

import application.authenticationManager.CustomAuthenticationManager;
import application.models.user.User;
import application.services.impl.user.ManagerServiceImpl;
import application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserAuthentication implements CustomAuthenticationManager {

    private final UserService userService;
    private final PasswordEncoder encoder;
    private static final Logger logger = LoggerFactory.getLogger(ManagerServiceImpl.class);

    public UserAuthentication(UserService userService, PasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = userService.loadUserByUsername(username);
        if (user != null && user.getEmail().equals(username) && encoder.matches(password, user.getPassword())) {
            return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        }

        logger.info("Info" + authentication);
        throw new AuthenticationException("Введены неверные учетные данные") {
        };
    }
}
