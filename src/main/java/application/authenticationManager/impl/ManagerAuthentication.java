package application.authenticationManager.impl;

import application.authenticationManager.CustomAuthenticationManager;
import application.models.user.Manager;
import application.services.impl.user.ManagerServiceImpl;
import application.services.user.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class ManagerAuthentication implements CustomAuthenticationManager {

    private final ManagerService managerService;
    private final PasswordEncoder encoder;
    private static final Logger logger = LoggerFactory.getLogger(ManagerServiceImpl.class);

    public ManagerAuthentication(ManagerService managerService, PasswordEncoder encoder) {
        this.managerService = managerService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Manager manager = managerService.loadUserByUsername(username);
        if (manager != null && manager.getEmail().equals(username) && encoder.matches(password, manager.getPassword())) {
            return new UsernamePasswordAuthenticationToken(manager, null, manager.getAuthorities());
        }


        logger.info("Info" + authentication);
        throw new AuthenticationException("Неверные учетные данные") {
        };
    }
}
