package application.authenticationManager;

import org.springframework.security.authentication.AuthenticationManager;

public interface CustomAuthenticationManager extends AuthenticationManager {
}
