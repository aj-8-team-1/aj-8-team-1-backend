package application.services.impl.event;

import application.models.event.City;
import application.models.event.Country;
import application.repositories.event.CityRepository;
import application.repositories.event.CountryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTest {
    @Mock
    private CityRepository cityRepository;
    @Mock
    private CountryRepository countryRepository;
    @InjectMocks
    private CityServiceImpl cityService;

    private City city;
    private Country country;

    @BeforeEach
    void setUp() {
        city = City.builder()
                .title("Test City")
                .build();
        city.setId(1L);

        country = Country.builder()
                .title("Test Country")
                .build();
        country.setId(1L);
    }

    @Test
    void testGetByIdCity() {
        Long id = 1L;

        when(cityRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(city));

        City result = cityService.getByIdCity(id);

        verify(cityRepository, times(1)).findById(id);

        assertNotNull(result);
        assertEquals("Test City", result.getTitle());
    }

    @Test
    void testCreateCity() {
        String countryName = "Test Country";
        String cityName = "New City";

        when(countryRepository.findByTitle(countryName)).thenReturn(java.util.Optional.ofNullable(country));

        assertDoesNotThrow(() -> cityService.createCity(countryName, cityName));

        verify(cityRepository, times(1)).save(any(City.class));
    }

    @Test
    void testDeleteCity() {
        String cityName = "Test City";

        when(cityRepository.findByTitle(cityName)).thenReturn(java.util.Optional.ofNullable(city));

        assertDoesNotThrow(() -> cityService.deleteCity(cityName));

        verify(cityRepository, times(1)).delete(any(City.class));
    }

    @Test
    void testGetCitiesByCountry() {
        String countryName = "Test Country";
        List<City> cities = new ArrayList<>();
        cities.add(city);

        when(countryRepository.findByTitle(countryName)).thenReturn(java.util.Optional.ofNullable(country));
        when(cityRepository.findByCountry(country)).thenReturn(cities);

        List<String> result = cityService.getCitiesByCountry(countryName);

        verify(countryRepository, times(1)).findByTitle(countryName);
        verify(cityRepository, times(1)).findByCountry(country);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test City", result.get(0));
    }

    @Test
    void testGetAllCities() {
        Pageable pageable = Pageable.unpaged();
        List<City> cities = new ArrayList<>();
        cities.add(city);
        Page<City> cityPage = new PageImpl<>(cities);

        when(cityRepository.findAll(pageable)).thenReturn(cityPage);

        Page<City> result = cityService.getAllCities(pageable);

        verify(cityRepository, times(1)).findAll(pageable);

        assertNotNull(result);
        assertEquals(1, result.getTotalElements());
        assertEquals("Test City", result.getContent().get(0).getTitle());
    }

    @Test
    void testUpdateByIdCity() {
        Long id = 1L;
        City updatedCity = City.builder()
                .title("Updated City")
                .build();

        when(cityRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(city));

        assertDoesNotThrow(() -> cityService.updateByIdCity(updatedCity, id));

        verify(cityRepository, times(1)).findById(id);
        verify(cityRepository, times(1)).save(any(City.class));
    }
}
