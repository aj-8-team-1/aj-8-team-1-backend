package application.services.impl.event;

import application.models.event.Tag;
import application.repositories.event.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTest {
    @Mock
    private TagRepository tagRepository;
    @InjectMocks
    private TagServiceImpl tagService;

    private Tag tag;
    @BeforeEach
    void setUp(){
        tag = Tag.builder().title("test tag").build();
        tag.setId(1L);
    }
    @Test
    void testGetWebsiteByTitle(){
        when(tagRepository.findById(1L)).thenReturn(Optional.of(tag));

        Tag resultTag = tagService.getByIdTag(tag.getId());

        assertEquals(tag, resultTag);
    }
    @Test
    void testCreateTag(){
        tagService.createTag(tag);

        verify(tagRepository, times(1)).save(tag);
    }
    @Test
    void testUpdateByIdTag(){
        Tag existingTag = tag;

        Tag updatedTag = new Tag();
        updatedTag.setTitle("Updated Tag");
        updatedTag.setActive(false);

        when(tagRepository.findById(existingTag.getId())).thenReturn(Optional.of(existingTag));

        tagService.updateByIdTag(existingTag.getId(), updatedTag);

        assertEquals(updatedTag.getTitle(), existingTag.getTitle());
        assertEquals(updatedTag.isActive(), existingTag.isActive());

        verify(tagRepository, times(1)).save(any(Tag.class));
    }
    @Test
    void testGetTagList() {
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setTitle("test tag 1");

        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setTitle("test tag 2");

        List<Long> tagIds = Arrays.asList(1L, 2L);

        when(tagRepository.findById(1L)).thenReturn(Optional.of(tag1));
        when(tagRepository.findById(2L)).thenReturn(Optional.of(tag2));

        List<Tag> result = tagService.getTagList(tagIds);

        assertEquals(2, result.size());
        assertEquals(tag1, result.get(0));
        assertEquals(tag2, result.get(1));
    }
}
