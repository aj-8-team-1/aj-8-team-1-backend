package application.services.impl.event;

import application.DTO.events.EventRequestDTO;
import application.DTO.images.ImageDTO;
import application.eventsMail.EmailEvent;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.event.*;
import application.models.user.*;
import application.repositories.EventRequestRepository;
import application.services.event.*;
import application.services.user.ManagerService;
import application.services.user.UserService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EventRequestServiceImplTest {
    @Mock
    private EventRequestRepository repository;
    @Mock
    private EventService eventService;
    @Mock
    private CityService cityService;
    @Mock
    private OrganizerService organizerService;
    @Mock
    private EventCategoryService eventCategoryService;
    @Mock
    private TagService tagService;
    @Mock
    private WebsiteService websiteService;
    @Mock
    private ManagerService managerService;
    @Mock
    private PlaceService placeService;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationEventPublisher publisher;
    @Mock
    private ImageService imageService;
    @Mock
    private ImageMapper imageMapper;

    @InjectMocks
    private EventRequestServiceImpl eventRequestService;

    private EventRequest eventRequestFirst;
    private EventRequest eventRequestSec;
    private EventRequestDTO eventRequestDTO;
    private EventRequestDTO eventRequestDTOSec;
    private User user;
    private User userSec;

    @BeforeEach
    void setUp() {
        eventRequestFirst = EventRequest.builder()
                .title("Event Title")
                .description("Event Description")
                .format("Online")
                .comment("Event Comment")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(7))
                .status(EventRequestStatus.NEW)
                .city(new City())
                .websites(Arrays.asList(new Website(), new Website()))
                .eventCategory(new EventCategory())
                .tags(Arrays.asList(new Tag(), new Tag()))
                .images(Arrays.asList("new photo1", "new photo2"))
                .manager(new Manager())
                .place(new Place())
                .user(new User())
                .build();
        eventRequestFirst.setId(1L);
        eventRequestSec = EventRequest.builder()
                .title("Event Title2")
                .description("Event Description2")
                .format("Online")
                .comment("Event Comment2")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(3))
                .status(EventRequestStatus.NEW)
                .city(new City())
                .websites(Arrays.asList(new Website(), new Website()))
                .eventCategory(new EventCategory())
                .tags(Arrays.asList(new Tag(), new Tag()))
                .images(Arrays.asList("new photo3", "new photo4"))
                .manager(new Manager())
                .place(new Place())
                .user(new User())
                .build();
        eventRequestSec.setId(2L);
        user = User.builder()
                .email("user@example.com")
                .password("password")
                .firstName("John")
                .lastName("Doe")
                .authorities(Arrays.asList(new Authority(), new Authority()))
                .isEnabled(true)
                .provider(Provider.LOCAL)
                .userDetail(new UserDetail())
                .build();
        user.setId(1L);
        userSec = User.builder()
                .email("user2@example.com")
                .password("password2")
                .firstName("John2")
                .lastName("Doe2")
                .authorities(Arrays.asList(new Authority(), new Authority()))
                .isEnabled(true)
                .provider(Provider.LOCAL)
                .userDetail(new UserDetail())
                .build();
        userSec.setId(2L);
        eventRequestDTO = EventRequestDTO.builder()
                .title("Test Event")
                .description("This is a test event")
                .format("Online")
                .comment("Test comment")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(3))
                .cityIds(1L)
                .organizerId(1L)
                .websites(Arrays.asList("https://example.com", "https://test.com"))
                .categoryIds(1L)
                .tagIds(Arrays.asList(1L, 2L, 3L))
                .images(Arrays.asList("new photo5", "new photo6"))
                .placeId(1L)
                .build();
        eventRequestDTOSec = EventRequestDTO.builder()
                .title("Test Event2")
                .description("This is a test event2")
                .format("Online")
                .comment("Test comment2")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(3))
                .cityIds(1L)
                .organizerId(1L)
                .websites(Arrays.asList("https://example.com", "https://test.com"))
                .categoryIds(1L)
                .tagIds(Arrays.asList(1L, 2L, 3L))
                .images(Arrays.asList("new photo7", "new photo8"))
                .placeId(1L)
                .build();
    }

    @Test
    void testGetAllEvents() {
        List<EventRequest> eventRequests = new ArrayList<>();
        eventRequests.add(eventRequestFirst);
        eventRequests.add(eventRequestSec);

        Pageable pageable = PageRequest.of(0, 10);
        Page<EventRequest> page = new PageImpl<>(eventRequests, pageable, eventRequests.size());
        when(repository.findAll(pageable)).thenReturn(page);

        Page<EventRequest> result = eventRequestService.getAllEvents(pageable);

        assertEquals(page, result);
        verify(repository, times(1)).findAll(pageable);
    }

    @Test
    void testGetEventById() {
        Long id = 1L;
        when(repository.findById(id)).thenReturn(Optional.of(eventRequestFirst));

        EventRequest result = eventRequestService.getEventById(id);

        assertNotNull(result);
        assertEquals(eventRequestFirst, result);
        verify(repository, times(1)).findById(id);
    }

    @Test
    void testCreateEvent() {
        when(userService.loadUserByUsername(user.getEmail())).thenReturn(user);
        when(cityService.getByIdCity(eventRequestDTO.getCityIds())).thenReturn(new City());
        when(websiteService.getNewWebsites(eventRequestDTO.getWebsites())).thenReturn(new ArrayList<>());
        when(eventCategoryService.getByIdEventCategory(eventRequestDTO.getCategoryIds())).thenReturn(new EventCategory());
        when(tagService.getTagList(eventRequestDTO.getTagIds())).thenReturn(new ArrayList<>());
        when(placeService.getPlaceById(eventRequestDTO.getPlaceId())).thenReturn(new Place());

        eventRequestService.createEvent(eventRequestDTO, user.getEmail());

        verify(cityService, times(1)).getByIdCity(eventRequestDTO.getCityIds());
        verify(websiteService, times(1)).getNewWebsites(eventRequestDTO.getWebsites());
        verify(eventCategoryService, times(1)).getByIdEventCategory(eventRequestDTO.getCategoryIds());
        verify(tagService, times(1)).getTagList(eventRequestDTO.getTagIds());
        verify(placeService, times(1)).getPlaceById(eventRequestDTO.getPlaceId());
        verify(repository, times(1)).save(any(EventRequest.class));
    }

    @Test
    void testCreateEventRequestImages() {
        ImageDTO imageDTO = new ImageDTO();
        Long eventId = 1L;
        EventRequest eventRequest = new EventRequest();
        List<String> imageNames = Arrays.asList("new photo1", "new photo2");

        when(repository.findById(eventId)).thenReturn(java.util.Optional.of(eventRequest));
        when(imageMapper.toEntity(any(ImageDTO.class))).thenReturn(new Image());
        when(imageService.upload(any(Image.class))).thenReturn("new photo1");

        eventRequestService.createEventRequestImages(imageDTO, eventId);

        when(imageService.upload(any(Image.class))).thenReturn("new photo2");
        eventRequestService.createEventRequestImages(imageDTO, eventId);

        verify(repository, times(2)).findById(eventId);
        verify(imageMapper, times(2)).toEntity(any(ImageDTO.class));
        verify(imageService, times(2)).upload(any(Image.class));
        verify(repository, times(2)).save(eventRequest);
        assert eventRequest.getImages().equals(imageNames);
    }

    @Test
    void testStartProcessing() {
        String email = "manager@example.com";
        Manager manager = new Manager();
        when(managerService.loadUserByUsername(email)).thenReturn(manager);
        when(repository.findById(anyLong())).thenReturn(Optional.of(eventRequestSec));

        eventRequestService.startProcessing(eventRequestSec.getId(), email);

        assertEquals(EventRequestStatus.PROCESS, eventRequestSec.getStatus());
        assertEquals(manager, eventRequestSec.getManager());
        verify(repository, times(1)).save(eventRequestSec);
        verify(repository, times(1)).save(any());
        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }

    @Test
    void testApproveProcessing() {
        eventRequestSec.setStatus(EventRequestStatus.PROCESS);
        eventRequestSec.setUser(userSec);
        UserDetail userDetail = new UserDetail();
        userDetail.setCompany("Test Company");
        userDetail.setPhoneNumber("1234567890");
        userSec.setUserDetail(userDetail);
        when(repository.findById(eventRequestSec.getId())).thenReturn(Optional.of(eventRequestSec));
        when(organizerService.getByTitle("Test Company")).thenReturn(null);

        eventRequestService.approveProcessing(eventRequestSec.getId());

        assertEquals(EventRequestStatus.FINISH, eventRequestSec.getStatus());
        verify(repository, times(1)).save(eventRequestSec);
        verify(organizerService, times(1)).saveOrganizer(any(Organizer.class));
        verify(eventService, times(1)).save(any(Event.class));
        verify(repository, times(1)).deleteById(eventRequestSec.getId());
        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }

    @Test
    void testRejectProcessing() {
        eventRequestSec.setStatus(EventRequestStatus.PROCESS);
        eventRequestSec.setUser(userSec);
        when(repository.findById(eventRequestSec.getId())).thenReturn(Optional.of(eventRequestSec));

        eventRequestService.rejectProcessing(eventRequestSec.getId());

        verify(repository, times(1)).delete(eventRequestSec);
        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }

    @Test
    void testGetAllEventsByStatus() {
        EventRequestStatus status = EventRequestStatus.NEW;
        Pageable pageable = PageRequest.of(0, 10);
        List<EventRequest> eventRequests = new ArrayList<>();
        eventRequests.add(eventRequestFirst);
        eventRequests.add(eventRequestSec);
        Page<EventRequest> page = new PageImpl<>(eventRequests, pageable, eventRequests.size());
        when(repository.findByStatus(status, pageable)).thenReturn(page);

        Page<EventRequest> result = eventRequestService.getAllEventsByStatus(status, pageable);

        assertEquals(page, result);
        verify(repository, times(1)).findByStatus(status, pageable);
    }

    @Test
    void testDeleteEventRequest() {
        eventRequestSec.setStatus(EventRequestStatus.FINISH);
        when(repository.findById(eventRequestSec.getId())).thenReturn(Optional.of(eventRequestSec));

        eventRequestService.deleteEventRequest(eventRequestSec.getId());

        verify(repository, times(1)).deleteById(eventRequestSec.getId());
    }

    @Test
    void testDeleteEventById() {
        Long id = 1L;

        eventRequestService.deleteEventById(id);

        verify(repository, times(1)).deleteById(id);
    }

    @Test
    void testUpdateEventById() {
        Long id = 1L;
        EventRequest existingEvent = new EventRequest();
        when(repository.findById(id)).thenReturn(Optional.of(existingEvent));
        when(cityService.getByIdCity(eventRequestDTOSec.getCityIds())).thenReturn(new City());
        when(websiteService.getNewWebsites(eventRequestDTOSec.getWebsites())).thenReturn(new ArrayList<>());
        when(eventCategoryService.getEventCategoryByTitleIgnoreCase(eventRequestDTOSec.getTitle())).thenReturn(new EventCategory());
        when(tagService.getTagList(eventRequestDTOSec.getTagIds())).thenReturn(new ArrayList<>());

        eventRequestService.updateEventById(eventRequestDTOSec, id);

        assertEquals(eventRequestDTOSec.getTitle(), existingEvent.getTitle());
        assertEquals(eventRequestDTOSec.getDescription(), existingEvent.getDescription());
        assertEquals(eventRequestDTOSec.getFormat(), existingEvent.getFormat());
        assertEquals(eventRequestDTOSec.getComment(), existingEvent.getComment());
        assertEquals(eventRequestDTOSec.getStartDate(), existingEvent.getStartDate());
        assertEquals(eventRequestDTOSec.getEndDate(), existingEvent.getEndDate());

        verify(repository, times(1)).save(existingEvent);
    }

    @Test
    void testGetEventByIdThrowsException() {
        Long nonExistingId = 100L;
        when(repository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> eventRequestService.getEventById(nonExistingId));
    }

    @Test
    void testCreateEventWithEmptyLists() {
        EventRequestDTO eventRequestDTO = EventRequestDTO.builder()
                .title("Test Event")
                .description("This is a test event")
                .format("Online")
                .comment("Test comment")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(3))
                .cityIds(1L)
                .organizerId(1L)
                .websites(Collections.emptyList())
                .categoryIds(1L)
                .tagIds(Collections.emptyList())
                .images(Collections.emptyList())
                .placeId(1L)
                .build();

        when(userService.loadUserByUsername(user.getEmail())).thenReturn(user);
        when(cityService.getByIdCity(eventRequestDTO.getCityIds())).thenReturn(new City());
        when(websiteService.getNewWebsites(eventRequestDTO.getWebsites())).thenReturn(new ArrayList<>());
        when(eventCategoryService.getByIdEventCategory(eventRequestDTO.getCategoryIds())).thenReturn(new EventCategory());
        when(tagService.getTagList(eventRequestDTO.getTagIds())).thenReturn(new ArrayList<>());
        when(placeService.getPlaceById(eventRequestDTO.getPlaceId())).thenReturn(new Place());

        eventRequestService.createEvent(eventRequestDTO, user.getEmail());

        verify(repository, times(1)).save(any(EventRequest.class));
    }

    @Test
    void testSetUpdated() {
        EventRequest eventRequest = new EventRequest();
        assertNull(eventRequest.getUpdated());

        eventRequest.setUpdated();

        assertNotNull(eventRequest.getUpdated());
    }
}