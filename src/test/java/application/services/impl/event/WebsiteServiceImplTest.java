package application.services.impl.event;

import application.models.event.Website;
import application.repositories.event.WebsiteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class WebsiteServiceImplTest {

    @Mock
    private WebsiteRepository websiteRepository;

    @InjectMocks
    private WebsiteServiceImpl websiteService;
    private Website website;
    List<Website> websites;

    @BeforeEach
    void setUp(){
        website = Website.builder().title("test website").build();
        website.setId(1L);

        websites = new ArrayList<>();

        websites.add(website);
        websites.add(Website.builder().title("Website1").build());
        websites.add(Website.builder().title("Website2").build());

    }
    @Test
    void testGetWebsiteByTitle(){
        when(websiteRepository.getWebsiteByTitle(website.getTitle())).thenReturn(website);

        Website result = websiteService.getWebsiteByTitle(website.getTitle());

        assertEquals(website, result);
    }
    @Test
    void testGetNewWebsites(){
        List<String> websiteTitles = List.of("website1", "website2", "website3");

        when(websiteRepository.getWebsiteByTitle(anyString())).thenReturn(null);
        List<Website> result = websiteService.getNewWebsites(websiteTitles);

        assertEquals(3, result.size());
    }
}
