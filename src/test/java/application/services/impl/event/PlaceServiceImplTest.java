package application.services.impl.event;

import application.models.event.Place;
import application.repositories.event.PlaceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlaceServiceImplTest {
    @Mock
    private PlaceRepository placeRepository;
    @InjectMocks
    private PlaceServiceImpl placeService;

    private Place place;

    @BeforeEach
    void setUp() {
        place = new Place();
        place.setId(1L);
        place.setTitle("Test Place");
        place.setAddress("Test Address");
        place.setActive(true);
        place.getImages().add("image1.jpg");
        place.getImages().add("image2.jpg");

        //image = new PlaceImage();
    }
    @Test
    void testGetPlaceById() {
        when(placeRepository.findById(1L)).thenReturn(Optional.of(place));

        Place result = placeService.getPlaceById(1L);

        assertNotNull(result);
        assertEquals(place.getId(), result.getId());
        assertEquals(place.getTitle(), result.getTitle());
        assertEquals(place.getAddress(), result.getAddress());
        assertEquals(place.isActive(), result.isActive());
        assertEquals(place.getImages(), result.getImages());
    }

    @Test
    void testUpdatePlace() {
        Place existingPlace = new Place();
        existingPlace.setId(1L);
        existingPlace.setTitle("Existing Place");
        existingPlace.setAddress("Existing Address");
        existingPlace.setActive(true);

        Place updatedPlace = new Place();
        updatedPlace.setTitle("Updated Place");
        updatedPlace.setAddress("Updated Address");
        updatedPlace.setActive(false);

        when(placeRepository.findById(1L)).thenReturn(Optional.of(existingPlace));

        placeService.updatePlace(1L, updatedPlace);

        assertEquals(updatedPlace.getTitle(), existingPlace.getTitle());
        assertEquals(updatedPlace.getAddress(), existingPlace.getAddress());
        assertEquals(updatedPlace.isActive(), existingPlace.isActive());

        verify(placeRepository, times(1)).save(existingPlace);
    }
}
