package application.services.impl.event;

import application.models.event.Organizer;
import application.repositories.event.OrganizerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class OrganizerServiceImplTest {
    @Mock
    private OrganizerRepository organizerRepository;
    @InjectMocks
    private OrganizerServiceImpl organizerService;
    private Organizer organizer;

    @BeforeEach
    void setUp(){
        organizer = new Organizer();
        organizer.setTitle("test organizer");
        organizer.setPhoneNumber("test number");
        organizer.setId(1L);
    }
    @Test
    void testGetAll() {
        List<Organizer> expectedOrganizers = Arrays.asList(organizer);

        when(organizerRepository.findAll()).thenReturn(expectedOrganizers);

        List<Organizer> result = organizerService.getAll();

        assertEquals(expectedOrganizers.size(), result.size());
        assertTrue(result.containsAll(expectedOrganizers));
    }
    @Test
    void testByIdOrganizer(){
        when(organizerRepository.getOrganizerById(1L)).thenReturn(organizer);

        Organizer result = organizerService.getByIdOrganizer(organizer.getId());

        assertEquals(organizer, result);
    }
    @Test
    void testSaveOrganizer(){
        organizerService.saveOrganizer(organizer);

        verify(organizerRepository, times(1)).save(organizer);
    }
}
