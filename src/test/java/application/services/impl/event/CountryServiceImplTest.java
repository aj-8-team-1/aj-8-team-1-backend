package application.services.impl.event;

import application.models.event.City;
import application.models.event.Country;
import application.repositories.event.CityRepository;
import application.repositories.event.CountryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTest {
    @Mock
    private CountryRepository countryRepository;
    @Mock
    private CityRepository cityRepository;
    @InjectMocks
    private CountryServiceImpl countryService;

    private Country country;
    private City city;

    @BeforeEach
    void setUp() {
        country = Country.builder()
                .title("Test Country")
                .build();
        country.setId(1L);

        city = City.builder()
                .title("Test City")
                .build();
        city.setId(1L);
    }

    @Test
    void testGetGroupedCities() {
        List<Country> countries = new ArrayList<>();
        countries.add(country);

        List<City> cities = new ArrayList<>();
        cities.add(city);

        when(countryRepository.findAll()).thenReturn(countries);
        when(cityRepository.findByCountry(country)).thenReturn(cities);

        Map<String, List<String>> result = countryService.getGroupedCities();

        verify(countryRepository, times(1)).findAll();
        verify(cityRepository, times(1)).findByCountry(country);

        assertNotNull(result);
        assertTrue(result.containsKey("Test Country"));
        assertTrue(result.get("Test Country").contains("Test City"));
    }

    @Test
    void testGetAllCountries() {
        Pageable pageable = Pageable.unpaged();
        List<Country> countries = new ArrayList<>();
        countries.add(country);
        Page<Country> countryPage = new PageImpl<>(countries);

        when(countryRepository.findAll(pageable)).thenReturn(countryPage);

        Page<Country> result = countryService.getAllCountries(pageable);

        verify(countryRepository, times(1)).findAll(pageable);

        assertNotNull(result);
        assertEquals(1, result.getTotalElements());
        assertEquals("Test Country", result.getContent().get(0).getTitle());
    }

    @Test
    void testUpdateByIdCountry() {
        Long id = 1L;
        Country updatedCountry = Country.builder()
                .title("Updated Country")
                .build();

        when(countryRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(country));

        assertDoesNotThrow(() -> countryService.updateByIdCountry(updatedCountry, id));

        verify(countryRepository, times(1)).findById(id);
        verify(countryRepository, times(1)).save(any(Country.class));
    }

    @Test
    void testGetByIdCountry() {
        Long id = 1L;

        when(countryRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(country));

        Country result = countryService.getByIdCountry(id);

        verify(countryRepository, times(1)).findById(id);

        assertNotNull(result);
        assertEquals("Test Country", result.getTitle());
    }

    @Test
    void testCreateCountry() {
        String countryName = "New Country";

        when(countryRepository.findByTitle(countryName)).thenReturn(Optional.empty());

        assertDoesNotThrow(() -> countryService.createCountry(countryName));

        verify(countryRepository, times(1)).findByTitle(countryName);
        verify(countryRepository, times(1)).save(any(Country.class));
    }

    @Test
    void testDeleteCountry() {
        String countryName = "Test Country";

        when(countryRepository.findByTitle(countryName)).thenReturn(java.util.Optional.ofNullable(country));

        assertDoesNotThrow(() -> countryService.deleteCountry(countryName));

        verify(countryRepository, times(1)).findByTitle(countryName);
        verify(countryRepository, times(1)).delete(any(Country.class));
    }
}
