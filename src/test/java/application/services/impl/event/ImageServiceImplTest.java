package application.services.impl.event;

import application.exceptions.ImageUploadException;
import application.models.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ImageServiceImplTest {
    @InjectMocks
    private ImageServiceImpl imageService;

    private Image image;

    @BeforeEach
    void setUp() {
        image = new Image();
    }

    @Test
    void testUploadImageEmptyFile() {
        image.setFile(new MockMultipartFile("empty.jpg", new byte[0]));

        assertThrows(ImageUploadException.class, () -> imageService.upload(image));
    }
}
