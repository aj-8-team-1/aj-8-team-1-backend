package application.services.impl.event;

import application.DTO.events.EventDTO;
import application.DTO.images.ImageDTO;
import application.eventsSearch.EventSearchCriteria;
import application.mappers.ImageMapper;
import application.models.Image;
import application.models.event.*;
import application.repositories.event.EventRepository;
import application.services.event.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EventServiceImplTest {

    @Mock
    private EventRepository eventRepository;
    @Mock
    private CityService cityService;
    @Mock
    private OrganizerService organizerService;
    @Mock
    private EventCategoryService eventCategoryService;
    @Mock
    private TagService tagService;
    @Mock
    private WebsiteService websiteService;
    @Mock
    private PlaceService placeService;
    @Mock
    private ImageService imageService;
    @Mock
    private ImageMapper imageMapper;

    @InjectMocks
    private EventServiceImpl eventService;

    private Event event;
    private EventDTO eventDTO;

    @BeforeEach
    void setUp() {
        event = Event.builder()
                .title("Test Event")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(7))
                .description("Test description")
                .format("Online")
                .city(new City())
                .organizer(new Organizer())
                .websites(Collections.singletonList(new Website()))
                .eventCategory(new EventCategory())
                .tags(Collections.singletonList(new Tag()))
                .images(Arrays.asList("new photo1", "new photo2"))
                .place(new Place())
                .build();
        event.setId(1L);

        eventDTO = EventDTO.builder()
                .title("Test Event")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusDays(7))
                .description("Test description")
                .format("Online")
                .cityIds(1L)
                .organizerIds(1L)
                .website("https://example.com")
                .categoryIds(1L)
                .tagIds(Collections.singletonList(1L))
                .images(Arrays.asList("new photo3", "new photo4"))
                .placeId(1L)
                .build();
    }

    @Test
    void testGetAllEvents() {
        List<Event> events = Arrays.asList(event, event);
        Pageable pageable = PageRequest.of(0, 10);
        Page<Event> page = new PageImpl<>(events, pageable, events.size());
        when(eventRepository.findAll(pageable)).thenReturn(page);

        Page<Event> result = eventService.getAllEvents(pageable);

        assertEquals(page, result);
        verify(eventRepository, times(1)).findAll(pageable);
    }

    @Test
    void testCreateEvent() {
        when(cityService.getByIdCity(eventDTO.getCityIds())).thenReturn(new City());
        when(organizerService.getByIdOrganizer(eventDTO.getOrganizerIds())).thenReturn(new Organizer());
        when(websiteService.getWebsiteByTitle(eventDTO.getWebsite())).thenReturn(new Website());
        when(eventCategoryService.getByIdEventCategory(eventDTO.getCategoryIds())).thenReturn(new EventCategory());
        when(tagService.getTagList(eventDTO.getTagIds())).thenReturn(Collections.singletonList(new Tag()));
        when(placeService.getPlaceById(eventDTO.getPlaceId())).thenReturn(new Place());

        eventService.createEvent(eventDTO);

        verify(eventRepository, times(1)).save(any(Event.class));
    }

    @Test
    void testCreateEventImages() {
        ImageDTO imageDTO = new ImageDTO();
        Long eventId = 1L;
        Event event = new Event();
        List<String> imageNames = Arrays.asList("image1.jpg", "image2.jpg");

        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(event));
        when(imageMapper.toEntity(any(ImageDTO.class))).thenReturn(new Image());
        when(imageService.upload(any(Image.class))).thenReturn("image1.jpg");

        eventService.createEventImages(imageDTO, eventId);

        when(imageService.upload(any(Image.class))).thenReturn("image2.jpg");
        eventService.createEventImages(imageDTO, eventId);

        verify(eventRepository, times(2)).findById(eventId);
        verify(imageMapper, times(2)).toEntity(any(ImageDTO.class));
        verify(imageService, times(2)).upload(any(Image.class));
        verify(eventRepository, times(2)).save(event);
        assert event.getImages().equals(imageNames);
    }

    @Test
    void testGetEventById() {
        Long id = 1L;
        when(eventRepository.findById(id)).thenReturn(Optional.of(event));

        Event result = eventService.getEventById(id);

        assertEquals(event, result);
        verify(eventRepository, times(1)).findById(id);
    }

    @Test
    void testDeleteEventById() {
        Long id = 1L;

        eventService.deleteEventById(id);

        verify(eventRepository, times(1)).deleteById(id);
    }

    @Test
    void testUpdateEventById() {
        Long id = 1L;
        when(eventRepository.findById(id)).thenReturn(Optional.of(event));
        when(cityService.getByIdCity(eventDTO.getCityIds())).thenReturn(new City());
        when(organizerService.getByIdOrganizer(eventDTO.getOrganizerIds())).thenReturn(new Organizer());
        when(websiteService.getWebsiteByTitle(eventDTO.getWebsite())).thenReturn(new Website());
        when(eventCategoryService.getByIdEventCategory(eventDTO.getCategoryIds())).thenReturn(new EventCategory());
        when(tagService.getTagList(eventDTO.getTagIds())).thenReturn(Collections.singletonList(new Tag()));

        eventService.updateEventById(eventDTO, id);

        verify(eventRepository, times(1)).save(event);
    }

    @Test
    void testSearchEvents() {
        EventSearchCriteria criteria = new EventSearchCriteria();
        criteria.setTitle("Test Event");
        List<Event> events = Arrays.asList(event, event);
        when(eventRepository.findAll(any(Specification.class))).thenReturn(events);

        List<Event> result = eventService.searchEvents(criteria);

        assertEquals(events, result);
        verify(eventRepository, times(1)).findAll(any(Specification.class));
    }

    @Test
    void testSave() {
        eventService.save(event);

        verify(eventRepository, times(1)).save(event);
    }

    @Test
    void testGetEventsByCategory() {
        EventCategory eventCategory = new EventCategory();
        List<Event> events = Arrays.asList(event, event);
        when(eventRepository.findEventByEventCategory(eventCategory)).thenReturn(events);

        List<Event> result = eventService.getEventsByCategory(eventCategory);

        assertEquals(events, result);
        verify(eventRepository, times(1)).findEventByEventCategory(eventCategory);
    }
}