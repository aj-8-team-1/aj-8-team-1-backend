package application.services.impl.event;

import application.models.event.EventCategory;
import application.repositories.event.EventCategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EventCategoryServiceImplTest {
    @Mock
    private EventCategoryRepository eventCategoryRepository;
    @InjectMocks
    private EventCategoryServiceImpl eventCategoryService;
    private EventCategory eventCategory;

    @BeforeEach
    void setUp() {
        eventCategory = EventCategory.builder()
                .title("Test Event Category")
                .active(true)
                .build();
        eventCategory.setId(1L);
    }

    @Test
    void testGetAllEventCategory() {
        Pageable pageable = PageRequest.of(0, 10);

        List<EventCategory> eventCategories = Collections.singletonList(eventCategory);

        when(eventCategoryRepository.findAll(pageable)).thenReturn(new PageImpl<>(eventCategories));

        Page<EventCategory> result = eventCategoryService.getAllEventCategory(pageable);

        verify(eventCategoryRepository, times(1)).findAll(pageable);

        assertNotNull(result);
        assertEquals(1, result.getContent().size());
        assertEquals("Test Event Category", result.getContent().get(0).getTitle());
    }

    @Test
    void testGetActive() {
        List<EventCategory> activeCategories = Collections.singletonList(eventCategory);

        when(eventCategoryRepository.findByActiveTrue()).thenReturn(activeCategories);

        List<EventCategory> result = eventCategoryService.getActive();

        verify(eventCategoryRepository, times(1)).findByActiveTrue();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Event Category", result.get(0).getTitle());
    }

    @Test
    void testGetByIdEventCategory() {
        Long id = 1L;

        when(eventCategoryRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(eventCategory));

        EventCategory result = eventCategoryService.getByIdEventCategory(id);

        verify(eventCategoryRepository, times(1)).findById(id);

        assertNotNull(result);
        assertEquals("Test Event Category", result.getTitle());
    }

    @Test
    void testCreateEventCategory() {
        EventCategory newEventCategory = EventCategory.builder()
                .title("New Event Category")
                .active(true)
                .build();

        eventCategoryService.createEventCategory(newEventCategory);

        verify(eventCategoryRepository, times(1)).save(newEventCategory);
    }

    @Test
    void testDeleteByIdEventCategory() {
        Long id = 1L;

        eventCategoryService.deleteByIdEventCategory(id);

        verify(eventCategoryRepository, times(1)).deleteById(id);
    }

    @Test
    void testUpdateByIdEventCategory() {
        Long id = 1L;
        EventCategory updatedCategory = EventCategory.builder()
                .title("Updated Event Category")
                .active(false)
                .build();

        when(eventCategoryRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(eventCategory));

        eventCategoryService.updateByIdEventCategory(updatedCategory, id);

        verify(eventCategoryRepository, times(1)).findById(id);
        verify(eventCategoryRepository, times(1)).save(eventCategory);
    }

    @Test
    void testGetEventCategoryByTitleIgnoreCase() {
        String title = "Test Event Category";

        when(eventCategoryRepository.getEventCategoryByTitleIgnoreCase(title)).thenReturn(eventCategory);

        EventCategory result = eventCategoryService.getEventCategoryByTitleIgnoreCase(title);

        verify(eventCategoryRepository, times(1)).getEventCategoryByTitleIgnoreCase(title);

        assertNotNull(result);
        assertEquals("Test Event Category", result.getTitle());
    }
}
