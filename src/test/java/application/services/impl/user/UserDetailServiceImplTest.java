package application.services.impl.user;

import application.models.event.City;
import application.models.user.User;
import application.models.user.UserDetail;
import application.repositories.user.UserDetailRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserDetailServiceImplTest {

    @Mock
    private UserDetailRepository repository;

    @InjectMocks
    private UserDetailServiceImpl userDetailService;

    private UserDetail userDetail;
    private User user;

    @BeforeEach
    void setUp() {
        City city = new City();
        city.setTitle("New York");

        String image = "avatar.jpg";

        userDetail = new UserDetail();
        userDetail.setId(1L);
        userDetail.setImage(image);
        userDetail.setAge(25);
        userDetail.setGender("Male");
        userDetail.setPhoneNumber("1234567890");
        userDetail.setPosition("Engineer");
        userDetail.setCompany("ABC Company");
        userDetail.setCity(city);

        user = new User();
        user.setUserDetail(userDetail);
    }

    @Test
    void testGetUserDetailById() {
        Long userDetailId = 1L;
        when(repository.findById(userDetailId)).thenReturn(Optional.of(userDetail));

        UserDetail result = userDetailService.getUserDetailById(userDetailId);

        assertEquals(userDetail, result);
        verify(repository, times(1)).findById(userDetailId);
    }

    @Test
    void testGetUserDetailById_NotFound() {
        Long nonExistentId = 999L;
        when(repository.findById(nonExistentId)).thenReturn(Optional.empty());

        UserDetail result = userDetailService.getUserDetailById(nonExistentId);

        assertNull(result);
        verify(repository, times(1)).findById(nonExistentId);
    }

    @Test
    void testSaveUserDetail() {
        userDetailService.saveUserDetail(userDetail);

        verify(repository, times(1)).save(userDetail);
    }

    @Test
    void testDeleteUserDetail() {
        Long userDetailId = 1L;

        userDetailService.deleteUserDetail(userDetailId);

        verify(repository, times(1)).deleteById(userDetailId);
    }

    @Test
    void testUpdateUserDetail() {
        City city = new City();
        city.setTitle("Almaty");

        UserDetail updatedUserDetail = new UserDetail();
        updatedUserDetail.setAge(30);
        updatedUserDetail.setGender("Female");
        updatedUserDetail.setPhoneNumber("9876543210");
        updatedUserDetail.setPosition("Manager");
        updatedUserDetail.setCompany("XYZ Company");
        updatedUserDetail.setCity(city);

        userDetailService.updateUserDetail(user, updatedUserDetail);

        UserDetail updatedDetail = user.getUserDetail();
        assertEquals(updatedUserDetail.getAge(), updatedDetail.getAge());
        assertEquals(updatedUserDetail.getGender(), updatedDetail.getGender());
        assertEquals(updatedUserDetail.getPhoneNumber(), updatedDetail.getPhoneNumber());
        assertEquals(updatedUserDetail.getPosition(), updatedDetail.getPosition());
        assertEquals(updatedUserDetail.getCompany(), updatedDetail.getCompany());
        assertEquals(updatedUserDetail.getCity(), updatedDetail.getCity());

        verify(repository, times(1)).save(updatedDetail);
    }
}
