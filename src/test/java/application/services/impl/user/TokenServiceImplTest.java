package application.services.impl.user;

import application.models.user.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TokenServiceImplTest {
    @Mock
    private RedisTemplate<String, Token> redisTemplate;

    @Mock
    private ValueOperations<String, Token> valueOperations;
    @InjectMocks
    private TokenServiceImpl tokenService;
    private String teg;
    private Token token;
    @BeforeEach
    void setUp(){
        token = new Token(12345L);
        teg = "test teg";
    }
    @Test
    void testSet(){
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        tokenService.set(token, teg);

        verify(valueOperations).set(eq("test teg" + token.getToken()), eq(token));
    }
    @Test
    void testGet() {
        String key = teg + "12345";

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(key)).thenReturn(token);

        Optional<Token> result = tokenService.get("12345", teg);

        assertEquals(token, result.get());
    }
    @Test
    void testInvalidateToken() {
        tokenService.invalidateToken("12345", teg);

        verify(redisTemplate).delete("test teg12345");
    }
}
