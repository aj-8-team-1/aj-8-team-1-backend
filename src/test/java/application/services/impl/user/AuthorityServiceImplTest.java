package application.services.impl.user;

import application.models.user.Authority;
import application.repositories.user.AuthorityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthorityServiceImplTest {
    @Mock
    private AuthorityRepository repository;

    @InjectMocks
    private AuthorityServiceImpl authorityService;

    @Test
    void testFindAllByAuthority() {
        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("ADMIN"));
        authorities.add(new Authority("USER"));
        when(repository.findAllByAuthority("ADMIN")).thenReturn(authorities);

        List<Authority> result = authorityService.findAllByAuthority("ADMIN");

        assertEquals(2, result.size());
    }
    @Test
    void testFindByAuthority() {
        Authority authority = new Authority("ADMIN");

        when(repository.findByAuthority("ADMIN")).thenReturn(authority);

        Authority result = authorityService.findByAuthority("ADMIN");

        assertEquals(authority, result);
        verify(repository).findByAuthority("ADMIN");
    }
}
