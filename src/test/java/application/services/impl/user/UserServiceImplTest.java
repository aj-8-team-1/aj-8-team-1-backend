package application.services.impl.user;

import application.DTO.users.CreateUserDTO;
import application.DTO.users.UpdateUserDTO;
import application.eventsMail.EmailEvent;
import application.models.event.City;
import application.models.user.*;
import application.repositories.user.UserDetailRepository;
import application.repositories.user.UserRepository;
import application.services.user.AuthorityService;
import application.services.user.TokenService;
import application.services.user.UserDetailService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository repository;
    @Mock
    private AuthorityService authorityService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private TokenService tokenService;
    @Mock
    private ApplicationEventPublisher publisher;
    @Mock
    private UserDetailService userDetailService;
    @Mock
    private UserDetailRepository userDetailRepository;
    @Mock
    private OAuth2User oauth2User;

    @InjectMocks
    private UserServiceImpl userService;

    private User existingUser;
    private CreateUserDTO createUserDTO;
    private UpdateUserDTO updateUserDTO;
    private CustomOAuth2User customOAuth2User;

    @BeforeEach
    void setUp() {
        UserDetail userDetail = new UserDetail();
        userDetail.setImage("new photo1");
        userDetail.setAge(50);
        userDetail.setPhoneNumber("0123456789");
        userDetail.setPosition("engineer");
        userDetail.setCompany("company");
        userDetail.setCompanyConfirmation(false);
        userDetail.setCity(new City());
        userDetail.setId(1L);

        existingUser = User.builder()
                .email("test@example.com")
                .password("encodedPassword")
                .firstName("John")
                .lastName("Doe")
                .authorities(Collections.singletonList(new Authority()))
                .isEnabled(true)
                .provider(Provider.LOCAL)
                .userDetail(userDetail)
                .build();
        existingUser.setId(1L);

        createUserDTO = new CreateUserDTO();
        createUserDTO.setFirstName("Jane");
        createUserDTO.setLastName("Doe");
        createUserDTO.setEmail("jane@example.com");
        createUserDTO.setPassword("password");
        createUserDTO.setAuthority("USER");
        createUserDTO.setProvider("LOCAL");

        updateUserDTO = new UpdateUserDTO();
        updateUserDTO.setFirstName("Updated First Name");
        updateUserDTO.setLastName("Updated Last Name");

        customOAuth2User = new CustomOAuth2User(oauth2User);
    }

    @Test
    void testRegister_Success() {
        when(repository.existsUserByEmail(createUserDTO.getEmail())).thenReturn(false);
        when(authorityService.findAllByAuthority(createUserDTO.getAuthority())).thenReturn(Collections.singletonList(new Authority()));
        when(passwordEncoder.encode(createUserDTO.getPassword())).thenReturn("encodedPassword");

        userService.register(createUserDTO);

        verify(repository, times(1)).save(any(User.class));
        verify(userDetailService, times(1)).saveUserDetail(any(UserDetail.class));
        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }

    @Test
    void testRegister_UserAlreadyExists() {
        when(repository.existsUserByEmail(createUserDTO.getEmail())).thenReturn(true);

        assertThrows(IllegalArgumentException.class, () -> userService.register(createUserDTO));
        verify(repository, never()).save(any(User.class));
    }

    @Test
    void testLoadUserByUsername_Success() {
        when(repository.findByEmail(existingUser.getEmail())).thenReturn(Optional.of(existingUser));

        User result = userService.loadUserByUsername(existingUser.getEmail());

        assertEquals(existingUser, result);
    }

    @Test
    void testLoadUserByUsername_UserNotFound() {
        when(repository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("nonexistent@example.com"));
    }

    @Test
    void testFindAll() {
        List<User> users = List.of(existingUser);
        when(repository.findAll()).thenReturn(users);

        List<User> result = userService.findALl();

        assertEquals(users, result);
    }

    @Test
    void testEnable_Success() {
        String token = "validToken";
        Long userId = existingUser.getId();
        when(tokenService.get(token, "emails:")).thenReturn(Optional.of(new Token(userId)));
        when(repository.findById(userId)).thenReturn(Optional.of(existingUser));

        userService.enable(token);

        verify(repository, times(1)).save(existingUser);
        assertTrue(existingUser.isEnabled());
    }

    @Test
    void testEnable_InvalidToken() {
        String invalidToken = "invalidToken";
        when(tokenService.get(invalidToken, "emails:")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.enable(invalidToken));
        verify(repository, never()).save(any(User.class));
    }

    @Test
    void testEnable_UserNotFound() {
        String token = "validToken";
        Long nonExistentUserId = 999L;
        when(tokenService.get(token, "emails:")).thenReturn(Optional.of(new Token(nonExistentUserId)));
        when(repository.findById(nonExistentUserId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.enable(token));
        verify(repository, never()).save(any(User.class));
    }

    @Test
    void testResetPassword_Success() {
        String token = "validToken";
        String newPassword = "newPassword";
        Long userId = existingUser.getId();
        when(tokenService.get(token, "passwordReset:")).thenReturn(Optional.of(new Token(userId)));
        when(repository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(passwordEncoder.encode(newPassword)).thenReturn("encodedNewPassword");

        userService.resetPassword(token, newPassword);

        verify(repository, times(1)).save(existingUser);
        assertEquals("encodedNewPassword", existingUser.getPassword());
        verify(tokenService, times(1)).invalidateToken(token, "passwordReset:");
    }

    @Test
    void testResetPassword_InvalidToken() {
        String invalidToken = "invalidToken";
        String newPassword = "newPassword";
        when(tokenService.get(invalidToken, "passwordReset:")).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> userService.resetPassword(invalidToken, newPassword));
        verify(repository, never()).save(any(User.class));
        verify(tokenService, never()).invalidateToken(anyString(), anyString());
    }

    @Test
    void testResetPassword_UserNotFound() {
        String token = "validToken";
        String newPassword = "newPassword";
        Long nonExistentUserId = 999L;
        when(tokenService.get(token, "passwordReset:")).thenReturn(Optional.of(new Token(nonExistentUserId)));
        when(repository.findById(nonExistentUserId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.resetPassword(token, newPassword));
        verify(repository, never()).save(any(User.class));
        verify(tokenService, never()).invalidateToken(anyString(), anyString());
    }

    @Test
    void testRequestPasswordReset() {
        when(repository.findByEmail(existingUser.getEmail())).thenReturn(Optional.of(existingUser));

        userService.requestPasswordReset(existingUser.getEmail());

        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }

    @Test
    void testProcessOAuthPostLogin_NewUser() {
        when(repository.findByEmail(customOAuth2User.getEmail())).thenReturn(Optional.empty());
        when(authorityService.findAllByAuthority("USER")).thenReturn(Collections.singletonList(new Authority()));
        when(passwordEncoder.encode(any())).thenReturn("encodedPassword");

        userService.processOAuthPostLogin(customOAuth2User);

        verify(repository, times(1)).save(any(User.class));
        verify(userDetailService, times(1)).saveUserDetail(any(UserDetail.class));
    }

    @Test
    void testProcessOAuthPostLogin_ExistingUser() {
        when(repository.findByEmail(customOAuth2User.getEmail())).thenReturn(Optional.of(existingUser));

        userService.processOAuthPostLogin(customOAuth2User);

        verify(repository, never()).save(any(User.class));
        verify(userDetailService, never()).saveUserDetail(any(UserDetail.class));
    }

    @Test
    void testSave() {
        userService.save(existingUser);

        verify(repository, times(1)).save(existingUser);
    }

    @Test
    void testUpdateUser_Success() {
        when(repository.findByEmail(existingUser.getEmail())).thenReturn(Optional.of(existingUser));

        userService.updateUser(existingUser.getEmail(), updateUserDTO);

        assertEquals(updateUserDTO.getFirstName(), existingUser.getFirstName());
        assertEquals(updateUserDTO.getLastName(), existingUser.getLastName());
        verify(repository, times(1)).save(existingUser);
    }

    @Test
    void testUpdateUser_UserNotFound() {
        when(repository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> userService.updateUser("nonexistent@example.com", updateUserDTO));
        verify(repository, never()).save(any(User.class));
    }

    @Test
    void testIsCompanyConfirmed() {
        UserDetail userDetail = existingUser.getUserDetail();

        Authority exponentAuthority = new Authority();
        exponentAuthority.setAuthority("EXPONENT");

        when(authorityService.findByAuthority("EXPONENT")).thenReturn(exponentAuthority);
        when(userDetailRepository.save(any(UserDetail.class))).thenReturn(userDetail);
        when(repository.save(any(User.class))).thenReturn(existingUser);

        userService.isCompanyConfirmed(existingUser);

        assertTrue(userDetail.isCompanyConfirmation());
        verify(userDetailService, times(1)).saveUserDetail(userDetail);
        verify(repository, times(1)).save(existingUser);
        assertTrue(existingUser.getAuthorities().stream().anyMatch(authority -> {
            String authorityName = authority.getAuthority();
            return authorityName != null && authorityName.equals("EXPONENT");
        }));
    }

    @Test
    void testAddRoleToUser_AuthorityNotFound() {
        when(authorityService.findByAuthority("EXPONENT")).thenReturn(null);

        userService.isCompanyConfirmed(existingUser);

        verify(repository, never()).save(any(User.class));
    }

    @AfterEach
    public void tearDown() {
        Mockito.reset(repository, authorityService, passwordEncoder, tokenService, publisher, userDetailService, userDetailRepository, oauth2User);
    }
}