package application.services.impl.user;

import application.DTO.managers.CreateManagerDTO;
import application.eventsMail.EmailEvent;
import application.models.user.*;
import application.repositories.user.ManagerRepository;
import application.services.user.AuthorityService;
import application.services.user.TokenService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ManagerServiceImplTest {

    @Mock
    private ManagerRepository repository;
    @Mock
    private AuthorityService authorityService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private TokenService tokenService;
    @Mock
    private ApplicationEventPublisher publisher;

    @InjectMocks
    private ManagerServiceImpl managerService;

    private Manager existingManager;
    private CreateManagerDTO createManagerDTO;

    @BeforeEach
    void setUp() {
        existingManager = Manager.builder()
                .email("test@example.com")
                .password("encodedPassword")
                .firstName("John")
                .lastName("Doe")
                .authorities(Collections.singletonList(new Authority()))
                .isEnabled(true)
                .build();
        existingManager.setId(1L);

        createManagerDTO = CreateManagerDTO.builder()
                .firstName("Jane")
                .lastName("Jane")
                .email("jane@example.com")
                .password("password")
                .authority("MANAGER")
                .build();
    }

    @Test
    void testRegister_Success() {
        when(repository.existsManagerByEmail(createManagerDTO.getEmail())).thenReturn(false);
        when(authorityService.findAllByAuthority(createManagerDTO.getAuthority())).thenReturn(Collections.singletonList(new Authority()));
        when(passwordEncoder.encode(createManagerDTO.getPassword())).thenReturn("encodedPassword");

        Manager result = managerService.register(createManagerDTO);

        assertNotNull(result);
        assertEquals(createManagerDTO.getFirstName(), result.getFirstName());
        assertEquals(createManagerDTO.getLastName(), result.getLastName());
        assertEquals(createManagerDTO.getEmail(), result.getEmail());
        assertNotNull(result.getPassword());
        verify(repository, times(1)).save(any(Manager.class));
    }

    @Test
    void testRegister_ManagerAlreadyExists() {
        when(repository.existsManagerByEmail(createManagerDTO.getEmail())).thenReturn(true);

        assertThrows(IllegalArgumentException.class, () -> managerService.register(createManagerDTO));
        verify(repository, never()).save(any(Manager.class));
    }

    @Test
    void testLoadUserByUsername_Success() {
        when(repository.findByEmail(existingManager.getEmail())).thenReturn(Optional.of(existingManager));

        Manager result = managerService.loadUserByUsername(existingManager.getEmail());

        assertEquals(existingManager, result);
    }

    @Test
    void testLoadUserByUsername_ManagerNotFound() {
        when(repository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> managerService.loadUserByUsername("nonexistent@example.com"));
    }

    @Test
    void testFindAll() {
        List<Manager> managers = List.of(existingManager);
        when(repository.findAll()).thenReturn(managers);

        List<Manager> result = managerService.findALl();

        assertEquals(managers, result);
    }

    @Test
    void testEnable_Success() {
        String token = "validToken";
        Long managerId = existingManager.getId();
        when(tokenService.get(token, "emails:")).thenReturn(Optional.of(new Token(managerId)));
        when(repository.findById(managerId)).thenReturn(Optional.of(existingManager));

        managerService.enable(token);

        verify(repository, times(1)).save(existingManager);
        assertTrue(existingManager.isEnabled());
    }

    @Test
    void testEnable_InvalidToken() {
        String invalidToken = "invalidToken";
        when(tokenService.get(invalidToken, "emails:")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> managerService.enable(invalidToken));
        verify(repository, never()).save(any(Manager.class));
    }

    @Test
    void testEnable_ManagerNotFound() {
        String token = "validToken";
        Long nonExistentManagerId = 999L;
        when(tokenService.get(token, "emails:")).thenReturn(Optional.of(new Token(nonExistentManagerId)));
        when(repository.findById(nonExistentManagerId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> managerService.enable(token));
        verify(repository, never()).save(any(Manager.class));
    }

    @Test
    void testResetPassword_Success() {
        String token = "validToken";
        String newPassword = "newPassword";
        Long managerId = existingManager.getId();
        when(tokenService.get(token, "passwordReset:")).thenReturn(Optional.of(new Token(managerId)));
        when(repository.findById(managerId)).thenReturn(Optional.of(existingManager));
        when(passwordEncoder.encode(newPassword)).thenReturn("encodedNewPassword");

        managerService.resetPassword(token, newPassword);

        verify(repository, times(1)).save(existingManager);
        assertEquals("encodedNewPassword", existingManager.getPassword());
        verify(tokenService, times(1)).invalidateToken(token, "passwordReset:");
    }

    @Test
    void testResetPassword_InvalidToken() {
        String invalidToken = "invalidToken";
        String newPassword = "newPassword";
        when(tokenService.get(invalidToken, "passwordReset:")).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> managerService.resetPassword(invalidToken, newPassword));
        verify(repository, never()).save(any(Manager.class));
        verify(tokenService, never()).invalidateToken(anyString(), anyString());
    }

    @Test
    void testResetPassword_ManagerNotFound() {
        String token = "validToken";
        String newPassword = "newPassword";
        Long nonExistentManagerId = 999L;
        when(tokenService.get(token, "passwordReset:")).thenReturn(Optional.of(new Token(nonExistentManagerId)));
        when(repository.findById(nonExistentManagerId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> managerService.resetPassword(token, newPassword));
        verify(repository, never()).save(any(Manager.class));
        verify(tokenService, never()).invalidateToken(anyString(), anyString());
    }

    @Test
    void testRequestPasswordReset() {
        when(repository.findByEmail(existingManager.getEmail())).thenReturn(Optional.of(existingManager));

        managerService.requestPasswordReset(existingManager.getEmail());

        verify(publisher, times(1)).publishEvent(any(EmailEvent.class));
    }
}
